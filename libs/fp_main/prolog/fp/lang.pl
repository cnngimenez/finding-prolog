/*   lang
     Author: Giménez, Christian.

     Copyright (C) 2016 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     18 nov 2016
*/


:- module(lang, [
	      scan_std_lib/1,
	      starting_lang/2
	  ]).
/** <module> lang: Language Scanner and Queries.

Predicates that allows to scan and query declarations from the primitives or the standard library of the language.

Currently, the implementation is to create a project with the same name as the language. 
*/

% :- ensure_loaded(library(prosqlite)).
% :- ensure_loaded(library(db_facts)).
% :- use_module(...).

:- use_module(library(apply)).

:- use_module(scanner).

/**
   load_lang_specs(+Language).

Load the language specifications and necessary predicates for scaning its 
standard libraries.
*/
load_lang_specs(Language) :-
    atomic_list_concat(
	['language_specs/', Language, '_spec'], Mod),
    display('Loading module:'), display(Mod), nl,
    use_module(Mod). 

/**
   scan_std_lib(+Language:term).

Scan for all declarations on the standard libraries of the given Language. 
There's must be defined std_lib_path/2 and std_lib_wildcard/2 for that
language.
*/
scan_std_lib(Language) :-
    load_lang_specs(Language),
    std_lib_paths(Language, Paths),
    std_lib_wildcard(Language, Wildcard),
    scan_all_paths(Language, Paths, Wildcard),
    nl,display('Done scanning std_lib').

scan_all_paths(_Language, [], _Wildcard). 
scan_all_paths(Language, [Path|Rest], Wildcard) :-
    scann_recursive(database(Language, Language), Path, Wildcard),!,
    scan_all_paths(Language, Rest, Wildcard).
    
/**
   starting_lang(+Language, +Str).

Ask for any functions on the standard library of the language.
*/
starting_lang(Language, Str) :-
    starting(database(Language, Language), Str).


