/*   queries
     Author: Giménez, Christian.

     Copyright (C) 2016 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     20 ago 2016
*/


:- module(queries, [
              default_db_path/1, create_db_path/0,
	      initialize/1, initialize/2,
	      search_starting/2,
              search_has_substring/2,
	      search_all_decs/2,
	      query_all_files/1,
              query_all_packages/1,

              filename_database/2,
              list_projects/2, list_projects/1,
              list_project_files/2, list_project_files/1,
              exists_project/2, exists_project/1
	  ]).
/** <module> queries: Queries which Answers to Stdout

Queries which answers are printed at the stdout.

# Example

```prolog
?- initialize('path/to/database-dir', database(ada, project_name)).
done.

?- search_starting(database(ada, project_name), myfunc).
...

?- finalize.
done.
```
*/

:- use_module(library(filesex), [make_directory_path/1]).
:- use_module(declarations).

%! default_db_path(-Path) is det.
%
% Where to store the databases?
%
% There should be only one default path!
default_db_path(Path) :-
    absolute_file_name('~/.local/share/finding-prolog/databases',
                       Path,
                       [expand(true)]).

%! create_db_path is det
%
% Creates the DB path and all subdirectories if they do not exists.
create_db_path :-
    default_db_path(Path),
    exists_directory(Path), !.
create_db_path :-
    default_db_path(Path),
    % syslog(info, 'Creating database subdirectories: ~w', [Path]).
    make_directory_path(Path).

print_str(Elt) :-
    atom_chars(Elt, L),
    (member(' ', L) ; member('\n', L)),
    display(Elt),!.
print_str(Elt) :-
    write('\''), write(Elt), write('\'').


%! print_term(+Term: term) is det.
%
% Print a term in quoted mode with a newline.
print_term(Term) :-
    writeq(Term), nl.

%! print_lst(+List: list) is det.
%
% Print to stdout a generic list of terms.
print_lst(List) :-
    maplist(print_term, List).

/** print_lstdecs(+LstDecs:list).

Print a list of declarations on screen.
*/
print_lstdecs([]).
print_lstdecs([dec(P, N, A, D, L, F)|Rest]) :-
    write('dec('),
    print_str(P), write(','),
    print_str(N), write(','),
    print_str(A), write(','),
    print_str(D), write(','),
    write(L), write(','),
    print_str(F), write(')'),nl,
    print_lstdecs(Rest).

/**
   print_lstfiles(+LstFiles:list).

Print a list of file/2 terms on the screen.
*/
print_lstfiles([]).
print_lstfiles([file(Filename, Date)|Rest]) :-
    write('file('),
    print_str(Filename), write(','),
    write(Date), write(')'),nl,
    print_lstfiles(Rest).
    

%! initialize(+DbConfig:term).
%
% Same as initialize/2 but using the default path. In other words:
%
%     initialize(Language:term, ProjName:term)
%
% @param DbConfig A database/2 term as `database(+Language:atom, +ProjName: atom)`.
%
% @see initialize/2
initialize(database(Language, ProjName)) :-
    default_db_path(DefaultPath),
    create_db_path,
    initialize(DefaultPath, database(Language, ProjName)).

%! initialize(+PathToDb:term, +DbConfig:term).
%! initialize(+Language:atom, +ProjName:atom).
%
% Initialize DB for the given Language and configure the path to the SQLite databases.
%
% The first call provides a different DB path. The second one uses the default DB path (see default_db_path/1).
%
% @param DbConfig A database/2 term as this: database(+Language:atom, +ProjName:atom).
% @param PathToDb A term that points to the path where the "db" directory is and all sqlite files inside.
initialize(PathToDb, database(Language, ProjName)) :- !,
    retractall(user:finding_prolog_dir(_)),
    assertz(user:finding_prolog_dir(PathToDb)),
    create_db(database(Language, ProjName)).
initialize(Language, ProjName) :-
    initialize(database(Language, ProjName)).

/** search_starting(+DbConfig:term, +Str:term).

Ask for any function that starts with the given Str string.

@param DbConfig A database/2 term as this: `database(+Language: term, +ProjName: term)`.
@param Str A prefix string to search for.
*/
search_starting(database(Language, ProjName), Str) :-
    starting_with(database(Language, ProjName), Str, LstDecs),
    print_lstdecs(LstDecs).

/** search_has_substring(+DbConfig:term, +Str:term).

Ask for any function that has the given Str string.
*/
search_has_substring(database(Language, ProjName), Str) :-
    has_substring(database(Language, ProjName), Str, LstDecs),
    print_lstdecs(LstDecs).

/** search_all_decs(+DbConfig:term, +Dec: term).

Dec must be a dec/4 with the following arguments:

    dec(?Name, ?Args, ?Desc, ?Line, ?Filepath).

We recommend to use at least one of the arguments unified for saving complexity
and processing time. 

Search all the declarations that matchs or can be unified by the given
arguments and display it.
*/
search_all_decs(database(Language, ProjName),
		        dec(Pack, Name, Args, Desc, Line, Filepath)) :-
    var(Filepath), !,
    all_declarations(database(Language, ProjName),
		             Pack, Name, Args, Desc, Line, Filepath, Lst),
    print_lstdecs(Lst).
search_all_decs(database(Language, ProjName),
		        dec(Pack, Name, Args, Desc, Line, Filepath)) :-
    absolute_file_name(Filepath, FullPath),
    all_declarations(database(Language, ProjName),
		             Pack, Name, Args, Desc, Line, FullPath, Lst),
    print_lstdecs(Lst).


/**
   query_all_files(+DbConfig).

Display all the files of the current project.
*/
query_all_files(database(Language,ProjName)) :-
    all_files(database(Language, ProjName), LstFiles),
    print_lstfiles(LstFiles).

%! query_all_packages(+DbConfig: term) is det.
%
% Print to stdout the package names in a project.
query_all_packages(database(Language, ProjName)) :-
    all_packs(database(Language, ProjName), LstPacks),
    print_lst(LstPacks).

%! filename_database(?Database: term, ?Filename: atom) is nondet.
%
% The Database term matches the given Filename.
% 
% This could be used to get the Filename from a database(Language, Project) predicate,
% to get the database/2 predicate from a Filename, or to check if the database/2 and
% Filename are correct.
filename_database(Filename, database(Language, Project)) :-
    %% atom_concat does not work in this order if Language or Project are variables.
    var(Filename), !,
    atom_concat(Language, '-', A1),
    atom_concat(A1, Project, A2),
    atom_concat(A2, '.sqlite', Filename), !.
filename_database(Filename, database(Language, Project)) :-
    atom_concat(A2, '.sqlite', Filename),
    atom_concat(A1, Project, A2),
    atom_concat(Language, '-', A1), !.

%! list_projects(+Directory: atom, ?ProjectList: list) is det.
% 
% Return a list of projects found on the given directory. ProjectList is a list of
% database/2 terms.
list_projects(Directory, ProjectList) :-
    list_project_files(Directory, FileList),
    maplist(filename_database, FileList, ProjectList).

%! list_projects(+ProjectList: list) is det
%
% List projects from the default database directory.
%
% This is the same as list_projects/2, but using the default directory.
%
% @see list_projects/2
list_projects(ProjectList) :-
    default_db_path(DefaultPath),
    list_projects(DefaultPath, ProjectList).


%! list_project_files(+Directory: atom, ?ProjectList: list) is det.
%
% Return the filenames that represents the projects.
list_project_files(Directory, ProjectList) :-
    atomic_concat(Directory, '/*-*.sqlite', Path),
    expand_file_name(Path, List),
    maplist(file_base_name, List, ProjectList).

%! list_project_files(?ProjectList: list) is det
%
% Return the project filenames from the default database path.
%
% This is the same as list_project_files/2, but using the default directory.
%
% @see list_project_files/2
list_project_files(ProjectList) :-
    default_db_path(DefaultPath),
    list_project_files(DefaultPath, ProjectList).

%! exists_project(?Database: term, +Directory: term) is nondet.
%
% True if the database(Language, Project) in Database exists as SQLite databes in Directory.
exists_project(database(Language, Project), Directory) :-
    filename_database(Filename, database(Language, Project)),
    atomic_list_concat([Directory, '/', Filename], FilePath),
    exists_file(FilePath).

%! exists_project(+Database: term) is det.
%
% Does the project given by Database exists in the default directory?
% 
% Same as exists_project/2 but using the default directory.
% 
% @param Database A `database(Language: atom, ProjectName: atom)` term.
% @see default_db_path/1
% @see exists_project/2
exists_project(Database) :-
    default_db_path(DefaultPath),
    exists_project(Database, DefaultPath).
