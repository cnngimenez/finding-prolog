/*   declarations
     Author: Christian Gimenez.

     Copyright (C) 2016 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     16 ago 2016
*/


:- module(declarations, [
	      lang_db_filename/2,
	      declaration/7,
	      file/3, all_files/2, add_file/3, delete_decs_file/2, file_unmodified/2,
              all_packs/2,
	      all_declarations/8,
	      create_db/1,
	      save_all/3,
	      starting_with/3, has_substring/3
	  ]).
/** <module> declarations: Basic DB predicates.
Provides the basic predicates and its access to the SQLite DB.

It is recommendend that a language is stored per each SQLite database file. For
example, LaTeX macros is stored as declarations in an SQLite file called
`latex.sqlite`, C functions at a `c.sqlite`, etc.

Different project can be stored using database/2 on DbConfig parameters. This 
compound terms has two arguments, the language name and the project name. For
example: `create_db(database(ada, myproject)).`.

```
?- assertz(finding_prolog_dir('/path/to/prolog/dir')).
true

?- create_db(database(ada, myproject)).
true

?- starting_with(database(ada, myproject), 'foo', LstRes).
LstRes=[ ... ]
```
*/

:- ensure_loaded(library(prosqlite)).
:- ensure_loaded(library(db_facts)).

%-Section Database creation predicates

/** lang_db_filename(DbConfig:term, DbFile:term).

true iff DbFile is the filepath for the given Language.

Remember to use `assertz(finding_prolog_dir('/path/here/')).' for configure where is the databases.
*/
lang_db_filename(database(Language, DbName), DbFile) :-
    user:finding_prolog_dir(FPDir),
    format(atom(DbFile), '~w/~w-~w.sqlite', [FPDir, Language,DbName]).

/** create_declaration_table.

true iff can create the declaration table at the SQLite database for the given Language database
or if the table already exist. 
*/
create_declaration_table :-
    db_table(database, declarations_tbl),!.
create_declaration_table :-    
    db_create(database, declarations_tbl(pack-text, name-text, params-text,
                                         desc-text, line+int, file_path+text)).

/** constraints(+DbConfig:term, +Dec:term).
constraints(+DbConfig:term, +File:term).

Is the given Dec declaration or File can be added?

Dec is a dec/6 term. File is a file/1 term. 
DbConfig is a database/2 term.

You can use this term to check safely before adding a term into the database.
*/
constraints(DbConfig, dec(_Pack, _Name, _Params, _Dec, Line, Filepath)) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),
    %% if db_holds/1 is true, then it already exists and it should fail.
    \+ db_holds(declarations_tbl(_, _, _, _, Line, Filepath)),
    db_disconnect(database),!.
constraints(DbConfig, file(Path, Date)) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),
    %% if db_holds/1 is true, then it already exists and it should fail.
    \+ db_holds(files_tbl(Path, Date)), 
    db_disconnect(database),!. 

constraints(_, _) :-
    db_disconnect(database),
    fail.

/** create_file_table.

true iff can create the file table at the SQLite database for the given Language database
or if the table already exists.
*/
create_file_table :-
    db_table(database, files_tbl), !.
create_file_table :-
    db_create(database, files_tbl(path+text, lastseen-timestamp)).

/** create_db(+DbConfig:term).

true iff can create a database with all its table for the given Language, or
true if the database is already created.

DbConfig is a compound term, database/2 as database(+Language:atom, +DbName:atom).
*/    
create_db(DbConfig) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database, exists(false)),
    create_declaration_table,
    create_file_table,
    db_disconnect(database).

%-Section Basic objects predicates.
%

%-Section Queries
%
/** row_to_dec(?LstRows, ?LstDecs).

true iff LstRows has all the row/5 predicates with the same information as the
dec/5 predicates of the list LstDecs.

Translate row/5 predicates from LstRows into dec/5 predicates to LstDecs, and
viceversa.
*/
row_to_dec([], []).
row_to_dec([row(Package, Name, Args, Desc, Line, File)|RRest],
           [dec(Package, Name, Args, Desc, Line, File)|DRest]) :-
    row_to_dec(RRest, DRest).

%! row_to_pack(?Row: term, Pack: term) is det.
%
% Convert a row/2 term into pack/2 term.
%
% The row/2 is a returned term from a database query.
row_to_pack(row(Pack, File), pack(Pack, File)).

%! rows_to_packs(+RowList: list, +PackList: list)
%
% Convert a list of row/2 to pack/2 terms.
%
% The row/2 terms are answers from a database query. By using findall/2, a list
% of such terms can be retrieved.
rows_to_packs(RowList, PackList) :-
    maplist(row_to_pack, RowList, PackList).

/** starting_with(+DbConfig:term, +Start:term, -LstRes:list).

true iff LstRes has all the dec/5 predicates obtained from the DB which name
starts with Start.
*/
starting_with(DbConfig, Start, LstRes) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),
    
    atomic_list_concat(['SELECT * FROM declarations_tbl WHERE name like "', Start, '%"'], Query),
    findall(Row, db_query(database, Query, Row), LstRow),
    row_to_dec(LstRow, LstRes),
    
    db_disconnect(database).

/** has_substring(+DbConfig:term, +Substring:term, -LstRes:list).

true iff LstRes has all the dec/6 predicates obtained from the DB which name
has the given substring.
*/
has_substring(DbConfig, Substring, LstRes) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),
    
    atomic_list_concat(['SELECT * FROM declarations_tbl WHERE name like "%', Substring, '%"'], Query),
    findall(Row, db_query(database, Query, Row), LstRow),
    row_to_dec(LstRow, LstRes),
    
    db_disconnect(database).

/** declaration(+DbConfig:term, ?Pack: term, ?Name:term, ?Parameters:term, ?Desc:term, ?Line:int, ?FilePath:term) is det

true iff declaration(Pa,N,P,D,L,F) exists on the database.
*/
declaration(DbConfig, Pack, Name, Parameters, Desc, Line, FilePath) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),
    
    ( db_holds(declarations_tbl(Pack, Name, Parameters, Desc, Line, FilePath)), ! ;
      %% if db_holds/1 fails, disconnect and then redo.
      db_disconnect(database), fail), 

    db_disconnect(database).

add_declaration(DbConfig, Pack, Name, Parameters, Desc, Line, FilePath, Desc) :-
    constraints(DbConfig, dec(Pack, Name, Parameters, Desc, Line, FilePath)),
    
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),
    db_assert(declarations_tbl(Pack, Name, Parameters, Desc, Line, FilePath)),!,    
    db_disconnect(database).

add_declaration(DbConfig, dec(Pack, Name, Parameters, Desc, Line), FilePath) :-
    constraints(DbConfig, dec(Pack, Name, Parameters, Desc, Line, FilePath)),
    
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),
    db_assert(declarations_tbl(Pack, Name, Parameters, Desc, Line, FilePath)),!,    
    db_disconnect(database).

/** all_declarations(+DbConfig:term, ?Name, ?Args, ?Desc, ?Line, ?FilePath, ?LstRes).

true iff LstRes is a list of dec/6 terms in the database that matchs the all of
the given Name, Args, Desc, Line and/or FilePath ( *if given instantiated* ).

*/
all_declarations(DbConfig, Pack, Name, Args, Desc, Line, FilePath, LstRes) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),
    
    findall(dec(Pack, Name, Args, Desc, Line, FilePath),
	    db_holds(declarations_tbl(Pack, Name, Args, Desc, Line, FilePath)),
	    LstRes),

    db_disconnect(database).

/** file(+DbConfig:term, +Path:term, ?Date:int) is det

true iff file(P) exists on the database.

@param DbConfig database/2 term setting language and project name.
@param Path The absolute path inside the database.
@param Date Is expressed in seconds since the Unix epoch, can be a variable.
*/
file(DbConfig, Path, Date) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),

    ( db_holds(files_tbl(Path, Date)), ! ; %% if db_holds/1 fails, disconnects and then redo.
      db_disconnect(database), fail ),
    
    db_disconnect(database).

/**
   file_was_modified(+DbConfig:term, +FilePath:term).

True iff the file exists in the DB and it was not modified since the last scan.

This means, that the file exists, also there's a tuple on the database stored
with this FilePath and the date stored is different from the file's date.
*/
file_unmodified(DbConfig, FilePath) :-
    time_file(FilePath, FileTime),
    file(DbConfig, FilePath, TimeStored),!,
    FileTime =:= TimeStored.

/**
   all_files(+DbConfig:term, -LstFiles: list).

true iff LstFiles is a list of file/2 terms of all the files stored in the DB.

@param DbConfig A database/2 term, see create_db/1.
@param LstFiles A list of file(Path, Date) term.
*/
all_files(DbConfig, LstFiles) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),

    findall(file(Path, Date),
	    db_holds(files_tbl(Path, Date)),
	    LstFiles),
    
    db_disconnect(database).

%! all_packs(+DbConfig:term, -LstPacks: list).
% 
% true iff LstPacks is a list of pack/2 terms of all the packages stored in the DB.
% 
% @param DbConfig A database/2 term, see create_db/1.
% @param LstPacks A list of pack(Name, FilePath) term.
all_packs(DbConfig, LstPacks) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),

    findall(Row, db_query(database,
                          'SELECT DISTINCT pack, file_path FROM declarations_tbl',
                          Row),
            LstRows),
    rows_to_packs(LstRows, LstPacks),
        
    db_disconnect(database).

/** add_file(+DbConfig:term, +File:term, +Date:int).

Add a File term into the database if it doesn't exists.

@param Date Must be a number of the seconds since the Unix epoch.
*/
add_file(DbConfig, File, Date) :-    
    constraints(DbConfig, file(File, Date)), !, % red cut

    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),    
    db_assert(files_tbl(File, Date)),
    db_disconnect(database).

add_file(_DbConfig, _File, _Date).

/** delete_decs_file(+DbConfig:term, +FilePath: term) is det.

Remove all declarations and the file entry from the given File.

Return true even when no FilePath exists in the database. Only false when the database doesn't exists.

Don't even think to rename this predicate to delete_file: it already exists!
*/
delete_decs_file(DbConfig, FilePath) :-
    lang_db_filename(DbConfig, Dbfile),
    sqlite_connect(Dbfile, database),
    db_retractall(files_tbl(FilePath, _Date)),
    db_retractall(declarations_tbl(_Pack, _Name, _Args, _Desc, _Line, FilePath)),
    db_disconnect(database).

/** save_all(+DbConfig:term, +File:term, +LstDecs:list).

Save declarations into the database selected by the DbConfig parameter.

File must be a file/2 term of format file(Path, Date). Date must be in seconds
since the Unix epoch.

@param LstDecs a list of terms dec(Pack, Name, Parameters, Desc, Line)
*/
save_all(DbConfig, file(FilePath, Date), LstDecs) :- 
    create_db(DbConfig),
    add_file(DbConfig, FilePath, Date),
    
    prepare_list(LstDecs, DbConfig, FilePath, LstDecs2),
    maplist(save_full_dec, LstDecs2).


/** save_full_dec(+FullDec).

FullDec is a fulldec/3 predicate. Where:

    fulldec(+DbConfig:term, +FilePath:term, +Dec).

Where Dec is a dec/5 term
dec(+Pack: term, +Name:term, +Params:term, +Desc:term, +Line).

true whenever FullDec could be saved or not. 
*/
save_full_dec(fulldec(DbConfig, FilePath, Dec)) :-
    ( add_declaration(DbConfig, Dec, FilePath),! ; true).

prepare_list([], _DbConfig, _FilePath, []).
prepare_list([Dec|Rest], DbConfig, FilePath,  [fulldec(DbConfig, FilePath, Dec)| FullRest]) :-
    prepare_list(Rest, DbConfig, FilePath, FullRest).
    
    

/** save_all_decs(+DbFile, +LstDecs:list, +FilePath:term).

Save a list of declarations terms. Used internally by save_all/3.

*Warning* : The database is expected to be opened!
*/
save_all_decs(_DbFile, [], _FilePath).
save_all_decs(DbFile, [dec(Pack, Name, Params, Desc, Line)|Rest], FilePath) :-
    sqlite_connect(DbFile, database),
    (db_holds(declarations_tbl(Pack, Name, Params, Desc, Line, FilePath)) ;
     db_disconnect(database), fail),
    db_disconnect(database),
    save_all_decs(DbFile, Rest, FilePath).
save_all_decs(DbFile, [dec(Pack, Name, Params, Desc, Line)|Rest], FilePath) :-
    sqlite_connect(DbFile, database),
    db_assert(declarations_tbl(Pack, Name, Params, Desc, Line, FilePath)),
    db_disconnect(database),
    save_all_decs(DbFile, Rest, FilePath).


