/*   file_scanner
     Author: Gimenez, Christian.

     Copyright (C) 2016 Gimenez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     19 ago 2016
*/


:- module(file_scanner, [
	      scann_file/3,
	      filter_empty_decs/2,
	      scann_next/8
	  ]	).
/** <module> file_scanner: File Scanner

Scan a file and report all identifiers found.


*/

:- ensure_loaded(library(readutil)).
% :- use_module(dcgs/ada).
:- use_module(library(fp/dcgs/commondcg)).
:- use_module(library(dcg/basics)).

/**
   filter_empty_decs(+LstDecs:list, -LstDecsFiltered:list) is det

Sometimes the scann_file/3 predicate create dec/5 elements with empty Params
and Desc. This predicate filters them in order to recieve a cleaner answer.

@param LstDecs A list of dec/5 terms each one with the following format: 
  `dec(Pack: string, Name:string, Params: string, Desc: string, LineNum:int)`
@param LstDecsFiltered A list of dec/5 terms.
*/
filter_empty_decs([], []).
filter_empty_decs([dec(_, "", "", _)|Rest], Rest2) :- !, % red cut
    filter_empty_decs(Rest, Rest2).
filter_empty_decs([Dec|Rest], [Dec|Rest2]) :-
    filter_empty_decs(Rest, Rest2).

/** load_lang_module(+Language).

Load the language from the dcgs directory.
*/	
load_lang_module(Language) :-
    atom_concat('fp/dcgs/', Language, Mod), 
    use_module(library(Mod)).


/** scann_file(+Language:atom, +File:atom, -LstDecs:list).

Scan for a file and report back the List of declarations.

LstDecs can return dec/5 atoms with empty string as Params and Desc. In such cases, those terms must be ignored.

@param LstDecs A list of dec/5 atoms: `dec(Pack: string, Name:string, Params:string, Desc:string, LineNum:int)`.
@param Language An atom describing the language. Example: `prologlang`, `ada`, etc.
@param File an atom with a relative or full path of the file to scan.
*/
scann_file(Language, File, LstDecs) :-
    load_lang_module(Language),
    open(File, read, Stream),
    read_stream_to_codes(Stream, LstCodes),
    close(Stream),
    atom_concat('main_', Language, Pred),
    amount_newlines(LstCodes, Total),
    scann_all(Pred, LstCodes, LstDecs1, 1, Total),
    filter_empty_decs(LstDecs1, LstDecs).

print_line(L, Total) :-
    format('|~w/~w', [L, Total]), flush_output.
print_line(_, _) :- !.


/** scann_all(+Pred:term, +LstCodes:list, -LstDecs:list, +Line:int, -Total:int).

Search for all declarations in LstCodes.

Call scann_next/8 as many times as required, to search for all declarations in LstCodes.

@param Pred The subprog_smth DCG predicate to call for matching the subprogram definitions.
@param LstCodes The characters from the file to scan.
@param LstDecs The list results from scanning of dec/5 predicates with the subprogram definitions.
@param Lines Should be 1. The starting line number counter.
@param Total An integer with the total newlines characters in the LstCodes. Use amount_newlines/2.
*/
scann_all(_Pred, [], [], _Line, _Total) :- !.
%% \-> Base case: LstCodes is empty.
scann_all(_Pred, _LstCodes, [], Total, Total) :- !.
%% \-> Base case: Total lines and current line is the same (EOF?).
scann_all(Pred, LstCodes, [Dec|DecRest], Line, Total) :-
    scann_next(Pred, LstCodes, Dec , Rest, Line, LineOut, Total, Command),
    (Command = stop; scann_all(Pred, Rest, DecRest, LineOut, Total)).
scann_all(_Pred, _, [], _Line, _Total).
%% \-> We don't want a false...


/** scann_next(+Pred:term, +LstCodes:list, -Dec:term, -Rest:list, +Line:int, -LineOut:int, +Total:int, -Command: term).

Find the next subprogram declaration in LstCodes using the DCG Pred.

true iff the next subprogram declaration is Dec and the rest
of the string after that is Rest. 
false if no other declaration is on LstCodes.

The subprogram for the language is selected through the Pred predicate.

Basically, find the next declaration.

Line must be an intenger that represents the starting line of LstCodes code
list of the entire file. It starts as 0, but each time scann_next/7 is called,
this number should advance in the succesive calls. LineOut is the number of
lines that the founded dec/4 finishs and Rest list codes starts. For example:

```prolog
?- scann_next(subprog_prologlang, `%% comment\nmypred(_A) :- true.\n%% morecomments`, Dec, Rest, 0, LineOut, Command).
Dec = dec("mypred", "_A", "comment", 1)
Rest = `%% morecomments`
LineOut = 3
Command = continue
```

@param Pred A predicate to call for searching the code. It should be a DCG, see subprog_prologlang//1.
@param LstCodes A code list to parse. You can load it with read_stream_to_codes/2.
@param Dec A dec/4 term of the resulting search, its format is: 
  `dec(Name:string, Args:string, Desc:string, LineNum:int)`.
@param Rest A code list, the last part of LstCodes, that wasn't parsed during the dec/4 search. 
@param Line An intenger which is considered the starting line number. 
@param LineOut An intenger which is considered the line number where the dec/4 finish (and Rest starts).
@param Total The amount of starting newlines character from the origina LstCodes.
@param Command The command red from the DCG predicate. It is `continue` or `stop` to keep or stop processing.
*/
scann_next(_Pred, LstCodes, dec("", "", "", 0), [], _Lines, Total, Total, continue) :-
    %% Base case, LstCodes are blanks only.
    dcg_basics:blanks(LstCodes, []).

scann_next(Pred, LstCodes, Dec, Rest, Line, LineOut, _Total, Command) :-
    %% Case: The language predicate matches a subprogram.
    call(Pred, Dec1, Command, LstCodes, Rest),!, % Red cut
    
    append(LstDecCodes, Rest, LstCodes),!,
    plus_line(LstDecCodes, Line, LineOut),
    %% print_line(Line, Total),
    
    append_linenum(Dec1, Line, Dec).

scann_next(Pred, LstCodes, Dec, Rest, Line, LineOut, Total, continue) :-
    %% Case: Language predicate did not match a subprogram.
    %% The, skip word and try again!
    word(LstCodes, LstRest2),
    append(Word, LstRest2, LstCodes),!,
    plus_line(Word, Line, Line2),
    %% format('>~w/~w', [Line, Total]), flush_output,
    scann_next(Pred, LstRest2, Dec, Rest, Line2, LineOut, Total, Command),
    Command \= stop.

/** append_linenum(+Dec: term, +LineNum: int, -DecOut: term)

Add the number of line where the declaration was founded.

@param Dec A package/1 or dec/3 term.
@param LineNum the number of line to add to the Dec term.
@param DecOut A package/2 or dec/4 term.
*/
append_linenum(package(Name), LineNum, package(Name, LineNum)) :- !.
append_linenum(dec(Name, Params, Comments), LineNum,
               dec(Name, Params, Comments, LineNum)).

/** plus_line(+LstCodes, +Line, -Line2)

Line2 is Line + 1 if LstCodes end with a new-line character, Line2 is Line
otherwise.
*/
% plus_line(LstCodes, Line, Line2) :-
%     append(_, `\n`, LstCodes), !, % red cut
%     Line2 is Line + 1.
% plus_line(_LstCodes, Line, Line).
plus_line(LstCodes, Line, Line2) :-
    amount_newlines(LstCodes, Amount),
    Line2 is Line + Amount.

/**
   amount_newlines(+LstCodes:list, -Amount:int).

Count the newlines characters in LstCodes.

True iff Amount is the amount of newlines in LstCodes.
*/
amount_newlines([], 0).
amount_newlines([10|Rest], Amount) :-
    amount_newlines(Rest, Amount2),!,
    Amount is Amount2 + 1.
amount_newlines([_|Rest], Amount) :-
    amount_newlines(Rest, Amount).
