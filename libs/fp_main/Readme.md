
# Finding Prolog - Main libraries

Example:

    ?- assertz(finding_prolog_dir('/home/path/to/finding-prolog/src')).
    true.
    
    ?- use_module(queries).
    true.
    
    ?- use_module(scanner).
    true.
    
    ?- use_module(lang).
    true.
    
    ?- initialize('/home/use/fp', database('php','project')).
    true.
    
    ?- scann_recursive(database('php','project'), '/home/user/project-dir/', '*.php').
    % recursive results here
    true.
    
    ?- starting(database('php','project'), 'foo').
    % Search for foo results here
    true.

The `assertz` sets the needed path to the SQLite databases, which are inside the `/home/path/to/finding-prolog/src/db/*.sqlite` in the example above.


# Running Tests

Load the modules that are inside the tests directory and run the tests by using the following predicate call:

    ?- run_tests.

See [Prolog Unit Tests package documentation](http://www.swi-prolog.org/pldoc/doc_for?object=section(%27packages/plunit.html%27)) for more information.

