* Finding Prolog - Main libraries

Example:

#+begin_src prolog
?- assertz(finding_prolog_dir('/home/path/to/finding-prolog/src')).
true.

?- use_module(queries).
true.

?- use_module(scanner).
true.

?- use_module(lang).
true.

?- initialize('/home/use/fp', database('php','project')).
true.

?- scann_recursive(database('php','project'), '/home/user/project-dir/', '*.php').
% recursive results here
true.

?- starting(database('php','project'), 'foo').
% Search for foo results here
true.
#+end_src


The =assertz= sets the needed path to the SQLite databases, which are inside the =/home/path/to/finding-prolog/src/db/*.sqlite= in the example above.

* Running Tests
Load the modules that are inside the tests directory and run the tests by using the following predicate call:

: ?- run_tests.

See [[http://www.swi-prolog.org/pldoc/doc_for?object=section(%27packages/plunit.html%27)][Prolog Unit Tests package documentation]] for more information.


* Meta     :noexport:

# ----------------------------------------------------------------------
#+TITLE:  FP Main libraries
#+SUBTITLE:
#+AUTHOR: Christian Gimenez
#+DATE:   15 ene 2023
#+EMAIL:
#+DESCRIPTION: 
#+KEYWORDS: 

#+STARTUP: inlineimages hidestars content hideblocks entitiespretty
#+STARTUP: indent fninline latexpreview

#+OPTIONS: H:3 num:t toc:nil \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t

# -- Export
#+LANGUAGE: en
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport


# --  Info Export
#+TEXINFO_DIR_CATEGORY: A category
#+TEXINFO_DIR_TITLE: FP Main libraries: (Readme)
#+TEXINFO_DIR_DESC: One line description.
#+TEXINFO_PRINTED_TITLE: FP Main libraries
#+TEXINFO_FILENAME: Readme.info


# Local Variables:
# org-hide-emphasis-markers: t
# org-use-sub-superscripts: "{}"
# fill-column: 80
# visual-line-fringe-indicators: t
# ispell-local-dictionary: "british"
# org-latex-default-figure-position: "tbp"
# End:
