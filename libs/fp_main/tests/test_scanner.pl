/*   test_scanner
     Author: Giménez, Christian.

     Copyright (C) 2017 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     17 ene 2017
*/


:- module(test_scanner, [	      
	  ]).
/** <module> test_scanner: Tests for scanner module.

Tests for scanner module.
*/

:- use_module(config).
initialize_fp_dir :-
    test_path(Path), test_dbconfig(DBConfig),
    nl, format('- Using path: ~w\n Configure test_path/1 to point to the "db" directory if this is not correct.', [Path]),
    nl, format('- Using DBConfig: ~w\n Change test_dbconfig/1 if language and project name is not correct.', [DBConfig]),

    absolute_file_name('example_test.pl', TestFile),
    retractall(user:test_file(_)), asserta(user:test_file(TestFile)),
    nl, format('- Example file is ~w\n', [TestFile]),

    retractall(user:finding_prolog_dir(_)), assertz(user:finding_prolog_dir(Path)).

:- begin_tests(scanner, [setup(initialize_fp_dir)]).
:- use_module('../prolog/fp/scanner').
:- use_module('../prolog/fp/declarations').

scann_setup :-
    create_db(Config),
    user:test_file(Testfile),
    delete_decs_file(Config, Testfile).
test(scann, [setup(scann_setup)]) :-
    test_dbconfig(Config),
    user:test_file(Testfile),
    
    scann(Config, './example_test.pl'),

    %%  some declarations that must be added.
    declaration(Config, 'config','test_dbconfig','-DBConfig:atom','',34,Testfile),

    declaration(
	Config, 'config',
	'test_path', A,'\n   test_path(-Path:term).\n\nChange this configuration to where you have the "db" directory where the SQLite database should be.\n\n@param Path A real path to the base directory where the database "db" directory is. \n',
	40,Testfile),
    A == '\'/home/christian/finding-prolog/src\'',
    
    declaration(
	Config, 'config', 'test_filepath','+Num:int, -FilePath:term','',
	50,Testfile),

    declaration(Config, 'config', 'test_filepath',B,'',	58,Testfile),
    B == '1, \'./config.pl\'',
    
    declaration(Config, 'config', 'test_filepath',C,'',	59,Testfile),
    C == '3, \'./test_queries.pl\''.

    
test(scann_nosave) :-
    scann_nosave(prologlang, './example_test.pl', Result),
    Result == [
	package("config",23),
	dec("test_dbconfig","-DBConfig:atom","",34),
	dec("test_path",
	    "'/home/christian/finding-prolog/src'",
	    "\n   test_path(-Path:term).\n\nChange this configuration to where you have the \"db\" directory where the SQLite database should be.\n\n@param Path A real path to the base directory where the database \"db\" directory is. \n",
	    40),
	dec("test_filepath","+Num:int, -FilePath:term","",50),
	dec("test_filepath","1, './config.pl'","",58),
	dec("test_filepath","2, 'another_file.adb'","",58),
	dec("test_filepath","3, './test_queries.pl'","",59)
    ].
		 
%% test(scann_recursive) :-

:- end_tests(scanner).

