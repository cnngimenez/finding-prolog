/*   test_declarations
     Author: Giménez, Christian.

     Copyright (C) 2017 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     17 ene 2017
*/


:- module(test_declarations, [	      
	  ]).
/** <module> test_declarations: Tests for declarations module.

Tests for declaration module.
*/


:- use_module('../prolog/fp/declarations').
:- use_module(config).

initialize_fp_dir :-
    test_path(Path), test_dbconfig(DBConfig),
    nl, format('- Using path: ~w\n Configure test_path/1 to point to the "db" directory if this is not correct.', [Path]),
    nl, format('- Using DBConfig: ~w\n Change test_dbconfig/1 if language and project name is not correct.', [DBConfig]),
    nl,
    retractall(user:finding_prolog_dir(_)), assertz(user:finding_prolog_dir(Path)).

:- begin_tests(declarations, [setup(initialize_fp_dir)]).

test(lang_db_filename) :-
    test_dbconfig(Config),
    lang_db_filename(Config, SQLiteFile),

    format('SQLite database full path: ~w\n', [SQLiteFile]),
    
    test_path(Path),    
    format(atom(SQLiteFile), '~w/db/prologlang-testproject.sqlite', [Path]).

test(create_db) :-
    test_dbconfig(DBConfig),
    
    create_db(DBConfig),

    lang_db_filename(DBConfig, SQLiteFile),
    exists_file(SQLiteFile).
  
test(add_file) :-
    test_filepath(1, File), test_filepath(2, File2),
    test_dbconfig(DBConfig),
    lang_db_filename(DBConfig, SQLiteFile),
    %% removing all
    ignore(delete_file(SQLiteFile)),
    create_db(DBConfig),
    
    %% testing adding the same file twice: it must not fail
    add_file(DBConfig, File, 1),
    add_file(DBConfig, File, 1),
    %% testing a new file
    add_file(DBConfig, File2, 2),
    
    %% tests their existence in the DB
    file(DBConfig, File, 1),
    file(DBConfig, File2, 2).

test(file) :-
    test_filepath(1, File),
    test_dbconfig(DBConfig),
    add_file(DBConfig, File, 1),

    %% test by date and filename
    file(DBConfig, File, 1),
    
    %% test for nonexistence
    \+ file(DBConfig, 'file_that_doesnot_exists', _).


%% It cannot became nondeteministic, db_holds/1 cannot work properly as soon the SQLite is disconnected.

%% test('file nondeterministic', [all(Number == [1,2])]) :-
%%     test_filepath(1, File), test_filepath(2, File2),
%%     test_dbconfig(DBConfig),
%%     add_file(DBConfig, File, 1),
%%     add_file(DBConfig, File2, 2),

%%     %% test filename existence
%%     file(DBConfig, _, Number).

/**
   Create the database and add three files.
   Also, deletes any files stored before.
*/
all_files_setup :-
    test_dbconfig(Config),
    test_filepath(1, F1),
    test_filepath(2, F2),
    test_filepath(3, F3),
    
    create_db(Config),
    delete_decs_file(Config, F1),
    delete_decs_file(Config, F2),
    delete_decs_file(Config, F3),

    add_file(Config, F1,1),
    add_file(Config, F2,2),
    add_file(Config, F3,3).

test(all_files, [setup(all_files_setup)]) :-
    test_dbconfig(Config),
    test_filepath(1, F1),
    test_filepath(2, F2),
    test_filepath(3, F3),

    all_files(Config, [
		  file(F1, 1),
		  file(F2, 2),
		  file(F3, 3)]).

/**
   declaration_setup is det

Create the database, add the second file on test_filepath/2 and insert some test declarations into the database.	
*/
declaration_setup :-
    test_dbconfig(Config),
    test_filepath(2, File),
    create_db(Config),
    add_file(Config, File, 2),
    save_all(Config, file(File, 2), [
		 dec(pack1, testdec, testparam, testdesc, 1),
		 dec(pack1, testdec2, testparam2, testdesc2, 2),
		 dec(pack1, testdec3, testparam3, testdesc3, 3)]).

test(declaration, [setup(declaration_setup)]) :-
    test_dbconfig(Config),
    test_filepath(2, File),
    declaration(Config, pack1, testdec, testparam, testdesc, 1, File).

%% We cannot test for nondeterministic behaviour.
%% Each declaration/6 call opens and closes the database providing the same answer or none after the first call.
%%
%% test(declaration_nondet, [setup(declaration_setup),
%% 			  all(dec(Name, Param, Desc, Line) == [
%% 				  dec(testdec, testparam, testdesc, 1),
%% 				  dec(testdec2, testparam2, testdesc2, 2),
%% 				  dec(testdec3, testparam3, testdesc3, 3)])
%% 			 ]) :-
%%     test_filepath(2, File),
%%     test_dbconfig(Config),
%%     declaration(Config, Name, Param, Desc, Line, File).
    

/**
   It must remove all declarations associated to a file.
*/
test(delete_decs_file, [setup(declaration_setup)]) :-
    test_dbconfig(Config),
    test_filepath(2, File),

    delete_decs_file(Config, File),
    
    \+ declaration(Config, pack1, testdec, testparam, testdesc, 1, File),
    \+ declaration(Config, pack1, testdec2, testparam2, testdesc2, 2, File),
    \+ declaration(Config, pack1, testdec3, testparam3, testdesc3, 3, File).

/**
if calling delete_decs_file/2 twice (when the File is note stored) it must not fail.
*/
test(delete_decs_file_nonexistant) :-
    test_dbconfig(Config),
    test_filepath(2, File),

    delete_decs_file(Config, File),
    delete_decs_file(Config, File).

/**
Add two files: one with the 
*/
file_unmodified_setup :-
    test_dbconfig(Config),
    test_filepath(1, File),
    test_filepath(3, File2),

    time_file(File, Filetime),
    create_db(Config),
    delete_decs_file(Config, File),
    delete_decs_file(Config, File2),
    add_file(Config, File, Filetime),
    add_file(Config, File2, 1).

test(file_unmodified,[setup(file_unmodified_setup)]) :-
    test_dbconfig(Config),
    test_filepath(1, File),
    test_filepath(3, File2),

    %% File exists and has not been modified.
    file_unmodified(Config, File),
    %% File exists and has been modified.
    \+ file_unmodified(Config, File2).
    
test(all_declarations, [setup(declaration_setup)]) :-
    test_dbconfig(Config),
    test_filepath(2, File),

    all_declarations(Config, _, _, _, _, _, File, [
		 dec(pack1, testdec, testparam, testdesc, 1, File),
		 dec(pack1, testdec2, testparam2, testdesc2, 2, File),
		 dec(pack1, testdec3, testparam3, testdesc3, 3, File)]).



save_all_setup :-
    test_dbconfig(Config),
    test_filepath(1, File),

    delete_decs_file(Config, File).

test(save_all, [setup(save_all_setup)]) :-
    test_dbconfig(Config),
    test_filepath(1, File),

    save_all(Config, file(File, 1), [
		 dec(pack1, save_all_testdec, save_all_testparam,
		     save_all_testdesc, 1),
		 dec(pack1, save_all_testdec2, save_all_testparam2,
		     save_all_testdesc2, 2),
		 dec(pack1, save_all_testdec3, save_all_testparam3,
		     save_all_testdesc3, 3)
	     ]),

    %% Check if the declarations really exists on the DB.
    all_declarations(
	Config, _, _, _, _, _, File, [
	    dec(pack1, save_all_testdec, save_all_testparam, save_all_testdesc,
		1, File),
	    dec(pack1, save_all_testdec2, save_all_testparam2, save_all_testdesc2,
		2, File),
	    dec(pack1, save_all_testdec3, save_all_testparam3, save_all_testdesc3,
		3, File)]).


starting_with_setup :-
    test_dbconfig(Config),
    test_filepath(1, File),

    delete_decs_file(Config, File),

    save_all(Config, file(File, 1), [
		 dec(pack1, starting_with_testdec, starting_with_testparam,
		     starting_with_testdesc, 1),
		 dec(pack1, starting_with_testdec2, starting_with_testparam2,
		     starting_with_testdesc2, 2),
		 dec(pack1, starting_with_testdec3, starting_with_testparam3,
		     starting_with_testdesc3, 3)
	     ]).
    
test(starting_with, [setup(starting_with_setup)]) :-
    test_dbconfig(Config),
    test_filepath(1, File),

    starting_with(Config, 'starting', [
		      dec(pack1, starting_with_testdec, starting_with_testparam,
			  starting_with_testdesc, 1, File),
		      dec(pack1, starting_with_testdec2, starting_with_testparam2,
			  starting_with_testdesc2, 2, File),
		      dec(pack1, starting_with_testdec3, starting_with_testparam3,
			  starting_with_testdesc3, 3, File)
		  ]).

test(has_substring, [setup(starting_with_setup)]) :-
    test_dbconfig(Config),
    test_filepath(1, File),

    has_substring(Config, 'with', [
		      dec(pack1, starting_with_testdec, starting_with_testparam,
			  starting_with_testdesc, 1, File),
		      dec(pack1, starting_with_testdec2, starting_with_testparam2,
			  starting_with_testdesc2, 2, File),
		      dec(pack1, starting_with_testdec3, starting_with_testparam3,
			  starting_with_testdesc3, 3, File)
		  ]).

:- end_tests(declarations).
