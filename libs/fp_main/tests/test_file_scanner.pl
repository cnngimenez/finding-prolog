/*   test_file_scanner
     Author: Giménez, Christian.

     Copyright (C) 2017 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     17 ene 2017
*/


:- module(test_file_scanner, [	      
	  ]).
/** <module> test_file_scanner: Tests for file_scanner module.

Tests for file_scanner module.
*/

:- begin_tests(file_scanner).
:- use_module('../prolog/fp/file_scanner').
:- use_module(library(fp/dcgs/prologlang)).
% :- use_module('../../fp_dcgs/prolog/dcgs/prologlang').

test(filter_empty_decs) :-
    filter_empty_decs([dec("A", "", "", 1),
		       dec("B", "B", "B", 2),
		       dec("C", "", "C", 3),
		       dec("D", "D", "", 4)],
		      [dec("B", "B", "B", 2),
		       dec("C", "", "C", 3),
		       dec("D", "D", "", 4)]).
		      
test(scann_file) :-
    scann_file(prologlang, './example_test.pl', Result),
    Result = [
	package("config",23),
	dec("test_dbconfig","-DBConfig:atom","",34),
	dec("test_path",
	    "'/home/christian/finding-prolog/src'",
	    "\n   test_path(-Path:term).\n\nChange this configuration to where you have the \"db\" directory where the SQLite database should be.\n\n@param Path A real path to the base directory where the database \"db\" directory is. \n",
	    40),
	dec("test_filepath","+Num:int, -FilePath:term","",50),
	dec("test_filepath","1, './config.pl'","",58),
	dec("test_filepath","2, 'another_file.adb'","",58),
	dec("test_filepath","3, './test_queries.pl'","",59)
    ].

test(scann_file_b) :-
    scann_file(ada, './examples/example.adb', Result),
    write(Result),
    Result = [
        package("My_Package.Another_Package.Example", 22),
        dec("Append_Strings",
            "Prefix, Suffix : String; Appened : out String",
            "", 26),
        dec("Append_Fnc",
            "Prefix, Suffix : String",
            "", 32)        
    ].


% We let scann_next to be nondeterministic, because the DCGs implemented can be nondet too.
test(scann_next, [nondet]) :-
    String = `/** \ncomments\n\n*/\ntestpred(A) :- true.\n\n%% more comments`,

    scann_next(prologlang:main_prologlang, String, Dec, Rest, 1, LineOut, 0, continue),
						
    Dec = dec("testpred", "A", "\ncomments\n\n", 1),
    Rest = `\n\n%% more comments`,
    LineOut = 5.

:- end_tests(file_scanner).
