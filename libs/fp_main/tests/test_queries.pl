/*   test_queries
     Author: Giménez, Christian.

     Copyright (C) 2017 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     17 ene 2017
*/


:- module(test_queries, []).
/** <module> test_queries: Tests for the queries module.

Tests for the queries module.
*/

:- begin_tests(queries).
:- use_module('../prolog/fp/queries').
:- use_module(config).
:- use_module('../prolog/fp/scanner').


test(initialize) :-
    test_path(Path),
    initialize(Path, database('prologlang', 'testproject')),

    %% SQLite file must exists
    format(atom(SQLiteFile), '~w/prologlang-testproject.sqlite' , [Path]),
    exists_file(SQLiteFile),

    %% finding_prolog_dir/1 must be asserted
    user:finding_prolog_dir(Path).

%% See starting_with/3 from declarations.
%% test(starting) :-

%% See all_declarations/6 from declarations.
search_all_decs_setup :-
    test_path(Path),
    test_dbconfig(Config),
    initialize(Path, Config),
    scann(Config, './example_test.pl').

test(search_all_decs, [setup(search_all_decs_setup)]) :-
    test_dbconfig(Config),
    absolute_file_name('./example_test.pl', File),
    
    search_all_decs(Config, dec(_, _, _, _, _, File)).
    
test(query_all_files) :-
    test_dbconfig(Config),
    query_all_files(Config).

:- end_tests(queries).
