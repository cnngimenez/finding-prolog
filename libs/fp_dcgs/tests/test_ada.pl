/*   test_ada.pl
     Author: Giménez, Christian.

     Copyright (C) 2023 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     19 feb 2023
*/

:- module(test_ada, []).
/** <module> test_ada
*/

:- use_module(library(plunit)).
:- use_module(library(debug)).
:- use_module('../prolog/fp/dcgs/ada').

:- begin_tests(ada_dcgs).

test(arguments1) :-
    phrase(arguments(Args), `(My_Variable : Integer; Another_Variable : in out String)`, R),
    Args = `My_Variable : Integer; Another_Variable : in out String`,
    R = [].

test(arguments2) :-
    phrase(arguments(Args),
           `(My_Variable :  Integer;\n Another_Variable : in out String)`, R),
    Args = `My_Variable : Integer; Another_Variable : in out String`,
    R = [].

%% Code obtained from Ada.Strings.Fixed (a-strfix.ads) from the GNAT project.
test(subprog) :-
    phrase(subprog_ada(Dec),`
    function Index
     (Source  : String;
      Pattern : String;
      From    : Positive;
      Going   : Direction := Forward;
      Mapping : Maps.Character_Mapping_Function) return Natural`, R),
    Dec = dec("Index", "Source : String; Pattern : String; From : Positive; Going : Direction := Forward; Mapping : Maps.Character_Mapping_Function", ""),
    R = ` return Natural`.

test(private) :-
    main_ada(dec("", "", ""), stop,
             `    private    \n  type Something is...`,
             `type Something is...`).

:- end_tests(ada_dcgs).

:- initialization(run_tests, main).
