/*   test_haskell.pl
     Author: Giménez, Christian.

     Copyright (C) 2023 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     25 june 2023
*/

:- module(test_haskell, []).
/** <module> test_haskell
*/

:- use_module(library(plunit)).
:- use_module(library(debug)).
:- use_module('../prolog/fp/dcgs/haskell').

:- begin_tests(haskell_dcgs).

test(module_without_parameters) :-
    phrase(main_haskell(A, B),
                 `    module The_Module where \n `,
                 ` \n `),
    A = package("The_Module"),
    B = continue.

test(module_with_parameters) :-
    phrase(main_haskell(A, B),
                 `    module The_Module (one, two, three) where \n `,
                 ` \n `),
    A = package("The_Module"),
    B = continue.

%% @todo Parse argument types too! Do not consider it as a string up until the end of line.
test(function_types) :-
    phrase(main_haskell(A, B),
                 `    main :: Int -> IO () \n `,
                 `\n `),
    A = dec("main", " Int -> IO () ", ""),
    B = continue.

test(function_definition) :-
    phrase(main_haskell(A, B),
                 `    main x = putStrLn x \n `,
                 ` putStrLn x \n `),
    A = dec("main", "x ", ""),
    B = continue.

test(function_definition2) :-
    phrase(main_haskell(A, B),
                 `    main x y   z = putStrLn x \n `,
                 ` putStrLn x \n `),
    A = dec("main", "x y   z ", ""),
    B = continue.

:- end_tests(haskell_dcgs).
