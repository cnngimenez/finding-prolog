/*   test_commondcgs
     Author: Giménez, Christian.

     Copyright (C) 2017 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     05 abr 2017
*/


:- module(test_commondcgs, [	      
	  ]).
/** <module> test_commondcgs: 


*/

:- use_module('../prolog/dcgs/commondcg').

:- begin_tests(commondcgs).

test(remove_spaces) :-
    remove_spaces(`foo foo2 foo3`, `foo    foo2 \n\t    foo3\n`, []).


:- end_tests(commondcgs).
