/*   ada
     Author: Giménez, Christian.

     Copyright (C) 2016 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     19 ago 2016
*/


:- module(ada, [
	      identifier//1,
	      subprog_ada//1,
	      arguments//1,
	      arg_int//1,
	      id_symbol//1,
	      id_symbols//1,
          pack_ada//2,
          main_ada//2
	  ]).
/** <module> ada: Ada DCG

Ada DCG rules.
*/

:- ensure_loaded(library(dcg/basics)).
:- use_module(commondcg).

id_symbol(`_`) --> "_".
id_symbol([S]) --> letter(S).
id_symbol([S]) --> digit(S).

id_symbols(S) --> id_symbol(S).
id_symbols(S1) --> id_symbol(S), id_symbols(S2), {append(S, S2, S1)}.

identifier(Name) --> string(Name), \+ id_symbol(_), !.
%% identifier(Name) --> letter(A), id_symbols(Syms), {append([A], Syms, Name)}.

arg_int([A]) --> nonblank(A).
arg_int([]) --> blank.
arg_int([32, A|Args2]) --> blank, nonblank(A), !,
                               arg_int(Args2).
arg_int(Args) --> blank, arg_int(Args).
arg_int(Args) --> nonblank(A), arg_int(Args2),
                {append([A], Args2, Args)}.
arguments(Args) --> "(", arg_int(Args), ")".


comment_line(Text) --> "--", whites, string_without([10],Text).
comments(Text) --> comment_line(Text).
comments(Text) --> comment_line(Text1), blanks, comments(Rest), blanks,
		   {append([Text1, `\n`, Rest] , Text)}.

procedure_def(NameL, ArgsL) --> ("procedure" ; "function"), blanks, identifier(NameL), blanks, arguments(ArgsL), !.

/** subprog_ada//1

Match a subprogram for an Ada syntax. 

Remember that the parameter must return a dec/3 predicate with the following
parameters, all converted to string: Name and Arguments..
*/
subprog_ada(dec(Name, Args, Desc)) --> comments(DescL), blanks, procedure_def(NameL, ArgsL),!,
				       {string_codes(Name, NameL),
					string_codes(Args, ArgsL),
					string_codes(Desc, DescL)}.
subprog_ada(dec(Name, Args, "")) --> blanks, procedure_def(NameL, ArgsL),
				     {string_codes(Name, NameL), string_codes(Args, ArgsL)}.

/** pack_ada//1
Match a package declaration on a Body or Specification Ada library.

Return a package/1 term with the name of the package in strings:
`package(+Name:string)`.
*/
pack_ada(package(Name), stop) --> "package", blanks, "body", !, blanks,
                          string_without([32, 10, 13, 12], NameL),
                          blanks, "is",
                          {string_codes(Name, NameL)}.
pack_ada(package(Name), continue) --> "package", blanks,
                          string_without([32,10,13,12], NameL),
                          blanks, "is",
                          {string_codes(Name, NameL)}.
%! private//
%
% Private declaration part of a package specification.
private --> blanks, "private", blanks.

/** main_ada(-Entry: term)//

Matches a package or subprogram declarations.

@param Entry a package/1 or dec/3 element.
*/
main_ada(dec("", "", ""), stop) --> private.
main_ada(Entry, continue) --> subprog_ada(Entry).
main_ada(Entry, Command) --> pack_ada(Entry, Command).
