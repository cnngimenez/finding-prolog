/*   prologlang
     Author: Giménez, Christian.

     Copyright (C) 2016 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     10 sep 2016
*/


:- module(prologlang, [
	      subprog_prologlang//1,
	      main_prologlang//2
	  ]).
/** <module> prolog: Prolog DCG rules.


*/

% :- ensure_loaded(library(prosqlite)).
% :- ensure_loaded(library(db_facts)).
% :- use_module(...).


:- ensure_loaded(library(dcg/basics)).
:- use_module(commondcg).


idint_symbol(`\\'`) --> [92,39], !. %% '
idint_symbol([C]) --> [C], {C\=39}.

ident_internal(Id) --> idint_symbol(S), ident_internal(Id2),!,
					{append(S, Id2, Id)}.
ident_internal(Id) --> idint_symbol(Id).


id_symbol(`_`) --> "_".
id_symbol([S]) --> letter(S).
id_symbol([S]) --> digit(S).
% id_symbol(`\'`) --> "\'".

ident1_int(Id) --> id_symbol(S), ident1_int(Id2),!,
				 {append(S, Id2, Id)}.
ident1_int(Id) --> id_symbol(Id).


comp_ident(Id) --> "'", !, ident_internal(Id2), "'",
		   {format(codes(Id), '\'~s\'', [Id2])}.

module_name(Id) --> comp_ident(Id),!.
module_name(Id) --> ident1_int(Id).

identifier(Id) --> module_name(Id1), ":", identifier(Id2),!,
		   {format(codes(Id), '~s:~s', [Id1, Id2])}.
identifier(Id) --> comp_ident(Id), !.
identifier(Id) --> ident1_int(Id).



% arg_int([A]) --> nonblank(A), {A \= 58}.
% arg_int([]) --> blank.
% arg_int(Args) --> nonblank(A), arg_int(Args2),
% 		  {A \= 58,
% 		   append([A], Args2, Args)}.
% arg_int(Args) --> blank, arg_int(Args).
% arguments(Args) --> "(", arg_int(Args), ")".
arguments(Args) --> "(", string_without(")", Args), ")".

% Ignore comments
% First thing to match.
whatever --> blank.
whatever --> nonblank(_).
whatever --> nonblank(_), whatever.
whatever --> blank, whatever.

whatever_to_nl --> eos.
whatever_to_nl --> "\n".
whatever_to_nl --> white, whatever_to_nl.
whatever_to_nl --> nonblank(_), whatever_to_nl.

% whatever_c(` `) --> white.
% whatever_c([C]) --> nonblank(C).
% whatever_c(Desc) --> nonblank(42), nonblank(C),
% 		   {C \= 47, append(`*`, C, Desc)}.
% whatever_c(Desc) --> white, whatever_c(Rest),
% 		   {append(` `, Rest, Desc)}.
% whatever_c(Desc) --> nonblank(A), whatever_c(Rest),
% 		   {A \= 42, append([A], Rest, Desc)}.
% comments(Desc) --> "/**", white, whatever_c(Desc), "*/".
comments(Desc) --> "/**", (whites, ! ; []) , string_without("*/", Desc), "*/", !.

/** comments_maybe//1

Matches a comment or the empty string if not possible.
*/
comments_maybe(Desc) --> comments(Desc), !. %% Red cut (if matches then do not match empty!)
comments_maybe(``) --> [].

args_maybe(ArgsL) --> arguments(ArgsL), !.
args_maybe(``) --> [].

head_parts(NameL, ArgsL) --> identifier(NameL), (blanks,! ; []), args_maybe(ArgsL), (blanks,! ; []).

body --> ".",!.
body --> ":-", string(_), ".".

% Ignore declarations inside body and comments.
% subprog_prologlang(dec("","","")) --> ":-", whatever_to_nl.
% subprog_prologlang(dec("","","")) --> "%", whatever_to_nl.
subprog_prologlang(dec("", "", "")) --> ":-", string_without([10], _), !, {fail}.
subprog_prologlang(dec("", "", "")) --> "%", string_without([10], _), !, {fail}.

% with arguments
subprog_prologlang(dec(Name, Args, Desc)) -->
    comments_maybe(DescL), blanks,
    head_parts(NameL, ArgsL), !,body,
    {string_codes(Name, NameL),
     string_codes(Args, ArgsL),
     string_codes(Desc, DescL)} .

% Ignore declarations inside comments.
% subprog_prologlang(dec("","","")) --> "/*", whatever, "*/".
subprog_prologlang(dec("","","")) --> "/*", string_without("*/", _), "*/", !, {fail}.

pack_prologlang(package(Name)) --> ":-", blanks, "module", blanks, "(", blanks,
				 module_name(NameL),
				 string_without(`.`, _), %% rest of the module statement
				 ".",
				 {string_codes(Name, NameL)}.

main_prologlang(Entry, continue) --> pack_prologlang(Entry), !. %% red cut
main_prologlang(Entry, continue) --> subprog_prologlang(Entry).
