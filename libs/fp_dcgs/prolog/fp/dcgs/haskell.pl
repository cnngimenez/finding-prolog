/*   haskell.pl
     Author: Gimenez, Christian.

     Copyright (C) 2023 Gimenez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     June 25, 2023
*/

:- module(haskell, [
              main_haskell//2,
              function_dec//1,
              module_dec//1
          ]).

/** <module> haskell: Haskell DCG for FP.

Haskell DCG rules to provide support for this language to
Finding-Prolog.
*/
:- ensure_loaded(library(dcg/basics)).
:- use_module(commondcg).

%! function_dec(?Entry: pred)// is det
%
% Parse a function type signature declaration or a function
% definition.
%
% @param Entry A dec/3 predicate: `dec(Name, Args, Description)`.
%
% @todo Parse the arguments with its own DCG rules too.
function_dec(dec(Name, Args, "")) -->
    %% Match the type declaration.
    blanks,
    string_without([32, 10, 13, 12, 58], NameL), blanks, "::", !,
    string_without([10, 13, 12], ArgsL),
    {string_codes(Name, NameL),
     string_codes(Args, ArgsL)}.
function_dec(dec(Name, Args, "")) -->
    %% Match the function definition.
    blanks,
    string_without([32, 10, 13, 12], NameL), blanks,
    string_without(`=`, ArgsL), blanks,
    "=", !,
    {string_codes(Name, NameL),
     string_codes(Args, ArgsL)}.

%! exports_name(?Exports: list)//
%
% Parse a list of exported names, separated with comas.
%
% @param Exports A list of exported names as strings.
exports_name([ExportName, Rest]) -->
    string_without([32, 10, 13, 12, 44, 41], ExportName),
    blanks, ",", !, blanks,
    exports_name(Rest).
exports_name([ExportName]) -->
    string_without([32, 10, 13, 12, 44, 41], ExportName), !.
exports_name([]) --> [].

%! module_args(?Export: list)// is det
%
% Parse the module export names.
%
% @param Export A list of strings with the exported names.
module_args(Exports) -->
    "(", !, blanks, exports_name(Exports), blanks, ")".
module_args([]) --> [].

%! module_dec(?ModuleEntry: pred)// is det
%
% Parse the module declaration from "module" up to "where" keywords.
%
% @param ModuleEntry A package/1 predicate: `package(Name)`.
module_dec(package(Name)) -->
    blanks, "module", blanks, string_without([32, 10, 13, 12, 40], NameL),
    blanks, module_args(_Exports),
    blanks, "where",
    {string_codes(Name, NameL)}.

%! main_haskell(-Entry: pred, -Continue: atom)// is det
%! main_haskell(?Entry: pred, ?Continue: atom)// is det
%
% Parse the haskell code looking for a module or function declaration.
%
% @param Entry    A package/1 or dec/3 entry.
% @param Continue continue or stop atom. Indicate if the parsing
%                 should stop or continue.
main_haskell(ModuleEntry, continue) -->
    %% First the module. The keyword "module" is faster to recognize
    %% than a function definition.
    module_dec(ModuleEntry), !.
main_haskell(FuncEntry, continue) -->
    function_dec(FuncEntry).
