/*   php
     Author: Giménez, Christian.

     Copyright (C) 2016 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     15 dic 2016
*/


:- module(php, [
	      subprog_php//1
	  ]).
/** <module> php: PHP DCGS

DCGS predicates for parsing PHP declarations.
*/

:- use_module(library(dcg/basics)).
:- use_module(commondcg).


comments(Desc) --> "/**", string_without("*/", Desc), "*/", !.
comments_maybe(Desc) --> comments(Desc), !. %% red cut
comments_maybe(``) --> [].

reserved_word --> "protected".
reserved_word --> "public".
reserved_word --> "abstract".

declaration_maybe --> reserved_word, whites,!.
declaration_maybe --> [].

subprog_php(dec(Name, Arg, Desc)) -->
    comments_maybe(DescL), blanks,
    declaration_maybe, "function", whites, string(NameL), (whites ; []), "(", string(ArgL), ")", !,
    {string_codes(Name, NameL),
     string_codes(Desc, DescL),
     string_codes(Arg, ArgL)}.
subprog_php(dec(Name, "", Desc)) -->
    comments_maybe(DescL), blanks,
    declaration_maybe, "function", whites, string(NameL), (whites ; []), "(", (whites ; []) , ")", !,
    {string_codes(Name, NameL),
     string_codes(Desc, DescL)}.

