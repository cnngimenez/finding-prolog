/*   elisp
     Author: Giménez, Christian.

     Copyright (C) 2018 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     02 abr 2018
*/


:- module(elisp, [
	      subprog_elisp//1
	  ]).
/** <module> elisp: Elisp support for finding prolog.


*/

subprog_elisp(dec(Name, Arg, Desc)) -->
    "(", whites, "defun", whites, string(NameL),
    whites, "(", string(ArgL), ")",
    whites, string(DescL),
    !,
    {string_codes(Name, NameL),
     string_codes(Desc, DescL),
     string_codes(Arg, ArgL)}.
