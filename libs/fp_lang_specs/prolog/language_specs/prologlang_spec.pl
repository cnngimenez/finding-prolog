/*   prologlang
     Author: Giménez, Christian.

     Copyright (C) 2016 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     18 nov 2016
*/


:- module(prologlang_spec, [
	      std_lib_paths/2,
	      std_lib_wildcard/2
	  ]).
/** <module> prologlang: SWI Prolog Language Specs.


*/

% :- ensure_loaded(library(prosqlite)).
% :- ensure_loaded(library(db_facts)).
% :- use_module(...).

std_lib_paths(prologlang, [
		  '/usr/local/lib/swipl-7.3.27',
		  '~/lib/swipl/'
	      ]).

std_lib_wildcard(prologlang, '*.pl').

