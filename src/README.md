# proSQLite sqlite_disconnect fix
There's a problem using the `sqlite3_close` in the prosqlite.c file: statements created and backups have to be finalized before executing this command. Instead, `sqlite3_close_v2` doesn't have this requisite and can be called for disconnecting despite ongoing statements or backups.

Change `sqlite3_close` for `sqlite3_close_v2` on the `~/lib/swipl/pack/prosqlite/c/prosqlite.c` file, execute the script inside the prosqlite directory and compile using `make`.

