/*   test_declarations
     Author: Giménez, Christian.

     Copyright (C) 2016 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     27 ago 2016
*/


:- module(test_declarations, [
	      test_module_declarations/0,
	      test_create_db/0,
	      test_file/0,
	      test_add_file/0,
	      test_declaration/0	      
	  ]).
/** <module> declarations: Declaratiions test module


*/

% :- ensure_loaded(library(prosqlite)).
% :- ensure_loaded(library(db_facts)).

:- use_module(declarations).
:- ensure_loaded(library(prosqlite)).
:- ensure_loaded(library(db_facts)).

filepath(1, '/home/christian/docs/soft_libre/mis_proyectos/finding-prolog/src/tests/prueba.adb').
filepath(2, '/home/christian/docs/soft_libre/mis_proyectos/finding-prolog/src/tests/prueba2.adb').

test_module_declarations :-
    display(test_create_db), test_create_db,
    display(test_file), test_file,
    display(test_add_file), test_add_file,
    display(test_declaration), test_declaration,
    display(done).
    
test_create_db :-
    create_db(testlang),
    exists_file('db/testlang.sqlite').

test_file :-
    filepath(1, File),
    sqlite_connect('db/testlang', database),
    (
	db_holds(files_tbl(File)) ;
	db_assert(files_tbl(File))
    ),
    db_disconnect(database),
    file(testlang, File).

test_add_file :- 
    filepath(1, File), filepath(2, File2),
    %% removing all
    delete_file('db/testlang.sqlite'),
    create_db(testlang),
    %% testing an existing file
    add_file(testlang, File),
    add_file(testlang, File),
    %% testing a new file
    add_file(testlang, File2),
    %% tests their existence
    sqlite_connect('db/testlang', database),
    db_holds(files_tbl(File)),
    db_holds(files_tbl(File2)),
    db_disconnect(database).


test_declaration :-
    filepath(2, File),
    %% adding a declaration
    sqlite_connect('db/testlang', database),
    (%% Create the dec1 declaration if it doesn't exists on the db, if exist ignore then.
	db_holds(declarations_tbl(dec1,
			       params1,
			       12,
			       File)) ;
	db_assert(declarations_tbl(dec1,
				   params1,
				   12,
				   File))
    ),
    db_disconnect(database),
    declaration(testlang,
		dec1,
		params1,
		12,
		File).
