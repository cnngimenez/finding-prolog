/*   test_scanner
     Author: Giménez, Christian.

     Copyright (C) 2016 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     27 ago 2016
*/

:- module(test_scanner, [test_scanner/0]).
/** <module> scanner: Scanner Tests


*/

:- use_module('scanner').

test_scanner :-
    scann_nosave(testlang,
		 'tests/prueba.adb',
		 [dec("Proc1", "A,B:inInteger;C:outInteger", 26),
		  dec("Proc2", "A:inoutInteger", 35),
		  dec("Func1", "A:inInteger", 42),
		  dec("Func2", "A:outInteger", 47)]).
