#!/usr/bin/env swipl

/*   install_swipl
     Author: Gimenez, Christian.

     Copyright (C) 2017 Gimenez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     24 sep 2017
*/


:- module(install_swipl, [
              install_lib/1,
              install_all_dependencies/0,
              install_all_local_libs/0,
              install_fp/0
          ]).
/** <module> install_swipl: Install script

This is an install script and module. It can be used as a script
running it from the terminal, or as a module loading it inside the
swipl REPL.

To run it a shell script:

    $ swipl install_swipl.pl

To run it inside swipl REPL:

    ?- use_module(install_swipl).
    ?- install_fp.

The install_fp/0 is the main entry point: it installs all
Finding-Prolog libraries. The other predicates install libraries
separately.
*/

%% Call main/0 and then, it calls main/1.
%% First parameter is the predicate to call.
:- initialization(main, main).
opt_type(l, local_only, boolean).
opt_type(local_only, local_only, boolean).
opt_help(local_only, 'Install packages provided by this repository only.').

%! complete_name(+Libname, -Path) is det.
%
% From the library name, return the complete path. It assumes that
% the finding-prolog install path is at ~/finding-prolog/.
%
% For example, `complete_name(fp_dcgs, Path)` would unify Path with
% the atom: `'file:///home/USERNAME/finding-prolog/libs/fp_dcgs'`.
complete_name(Libname, Path) :-
    atom_concat('~/finding-prolog/libs/', Libname, Relpath),
    expand_file_name(Relpath, [Abspath]),
    atom_concat('file://', Abspath, Path).

%! install_lib(+LibName) is det.
%
% Install the local library. The library should be at the home of
% finding-prolog project diretory.
install_lib(LibName) :-
    format('Installing ~w library.',[LibName]), nl,
    complete_name(LibName, Path),
    pack_install(Path, [upgrade(true)]).

%! libname(?Library)
%
% Defines the library name to install.
libname(fp_dcgs).
libname(fp_lang_specs).
libname(fp_main).

%! install_all_local_libs is det.
%
% Install al local libraries provided by finding prolog.
install_all_local_libs :-
    forall(libname(Libname), install_lib(Libname)).

%! libdependecy(?Library).
%
% Dependencies used by Finding Prolog.
libdependency(db_facts).
libdependency(prosqlite).

%! install_dependency(+Libname) is det.
%
% Install a dependency displaying the corresponding message for the
% user.
install_dependency(Libname) :-
    format('Installing ~w add-on.', [Libname]), nl,
    pack_install(Libname).

%! install_all_dependencies is det.
%
% Install al local libraries provided by finding prolog.
install_all_dependencies :-
    forall(libdependency(Libname), install_dependency(Libname)).

%! install_fp is det.
%
% Main program for the install Finding-Prolog.
% Install all local libs into your system.
install_fp :-
    write('> Installing depedencies...'), nl,
    install_all_dependencies,
    write('> Installing all swipl libraries...'), nl,
    install_all_local_libs,
    write('> done.'), nl.

run_install(Options) :-
    member(local_only(true), Options), !,
    write('Local only indicated.'), nl,
    write('> Installing all swipl libraries...'), nl,
    install_all_local_libs,
    write('> done.'), nl.

run_install(_) :-
    install_fp.

main(ArgV) :-
    argv_options(ArgV, _Positional, Options),
    run_install(Options).
