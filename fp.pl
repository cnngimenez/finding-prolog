#!/usr/bin/env swipl

/*   fp.pl
     Author: Gimenez, Christian.

     Copyright (C) 2023 Gimenez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     1 nov 2023
*/

:- module(fp, []).

:- use_module(library(fp/queries)).
:- use_module(library(fp/scanner)).

:- initialization(main, main).

opt_type(f, force, boolean).
opt_type(s, scan, boolean).
opt_type(l, lang, atom).
opt_type(p, project, atom).
opt_type(r, recursive, boolean).
opt_type(q, query, atom).
opt_type('L', list_projects, boolean).

opt_help(force, 'Force scan, even if the file has not been modified.').
opt_help(scan, 'Scan files and parse them.').
opt_help(lang, 'Programming language to use for parsing.').
opt_help(project, 'Project name to save the parsed declarations.').
opt_help(recursive, 'Scan recursively.').
opt_help(query, 'Query the database.').
opt_help(list_projects, 'List languages and projects databases.').

%! fp_write(+Term: term) is det.
%
% Write to terminal a quoted term with a newline.
%
% Useful to use this with maplist/2.
fp_write(Term) :-
    writeq(Term), nl.

%! fp_list_projects is det.
%
% List to stdout the database/2 predicates of all projects.
fp_list_projects :-
    list_projects(ProjectList),
    maplist(fp_write, ProjectList).

%! fp_scan(+Options: list, +PathList: list)
%
% Implement the scanning parameters for fp.pl command-line.
fp_scan(Options, [Path, FileWildcards|_]) :-
    %% Implements the recursive scan.
    member(scan(true), Options),
    member(recursive(true), Options),
    member(lang(Language), Options),
    member(project(Project), Options),
    absolute_file_name(Path, AbsPath),

    format('Scanning ~s path recursively.', [AbsPath]), nl,
    
    scann_recursive(database(Language, Project), AbsPath, FileWildcards).

fp_scan(Options, [Path|_]) :-
    %% Implements the non-recursive scan.
    member(scan(true), Options),
    member(lang(Language), Options),
    member(project(Project), Options),
    absolute_file_name(Path, AbsPath),

    member(force(Force), Options),
    
    exists_file(AbsPath),
    format('Scanning ~s path non-recursively.', [AbsPath]), nl,
    
    scann(database(Language, Project), AbsPath, [force(Force)]).

%! fp_query(+Options: list, +SearchList: list)
%
% Execute the query commands.
fp_query(Options, [SearchString|_]) :-
    %% Implements the "start" query
    member(lang(Language), Options),
    member(project(Project), Options),
    member(query(start), Options),
    
    search_starting(database(Language, Project), SearchString).

fp_query(Options, [SearchString|_]) :-
    %% Implements the "substring" query
    member(lang(Language), Options),
    member(project(Project), Options),
    member(query(substring), Options),
    
    search_has_substring(database(Language, Project), SearchString).

fp_query(Options, _Pos) :-
    %% Implements the "files" query
    member(query(files), Options),
    member(lang(Language), Options),
    member(project(Project), Options),
    
    query_all_files(database(Language, Project)).

fp_query(Options, [PackName|_]) :-
    %% Implements the "pack" query
    member(lang(Language), Options),
    member(project(Project), Options),
    member(query(pack), Options),
    
    search_all_decs(database(Language, Project), dec(PackName, _, _, _, _, _)).

fp_query(Options, _) :-
    %% Implements the "listpack" query
    member(lang(Language), Options),
    member(project(Project), Options),
    member(query(listpacks), Options),

    query_all_packages(database(Language, Project)).

%! fp_command(+Options: list, +Pos: list)
%
% Run the command provided by the parameters in Options.
fp_command(Options, _Pos) :-
    member(list_projects(true), Options), !,
    fp_list_projects.
fp_command(Options, Pos) :-
    member(scan(true), Options) , !,
    fp_scan(Options, Pos).
fp_command(Options, Pos) :-
    member(query(_), Options), !,
    fp_query(Options, Pos).
fp_command(_, _) :-
    write('Command not recognised.').

%! main(+ArgV: list)
%
% Main predicate for fp.pl command-line script.
main(ArgV) :-
    %% Implements case: -l and -p are provided.
    argv_options(ArgV, Positional, Options),

    member(lang(Language), Options),
    member(project(Project), Options),
    initialize(Language, Project),
    
    fp_command(Options, Positional).
main(ArgV) :-
    %% No -l nor -p are provided... perhaps -L?
    argv_options(ArgV, Positional, Options),
    fp_command(Options, Positional).
