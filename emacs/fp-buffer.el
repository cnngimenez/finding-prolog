;;; fp-buffer.el --- Side view buffer for FP results -*- lexical-binding: t; -*-

;; Copyright 2017 Gimenez, Christian
;;
;; Author: Gimenez, Christian
;; Version: 0.1
;; Keywords: tools, convenience
;; URL: https://gitlab.com/cnngimenez/finding-prolog
;; Package-Requires: ((emacs "27.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:
;; 
;; Provides with a personalised buffer for displaying FP results.
;;
;; The fp-buffer is a side view with the last FP results shown in a
;; user-friendly way.

;;; Code:

(require 'fp-results-mode)

(defcustom fp-buffer-in-buffer nil
  "Should the fp-buffer be in another window or just another buffer?
* nil it will start in another window.
* t in a buffer.")

(defconst fp-buffer-name "*fp-results*"
  "The buffer name.")

(defvar fp-buffer-last-command nil
  "This is the last command executed that called the buffer.
Must be setted before showing the buffer.
Used in `fp-buffer-update'.")


(defun fp-buffer-create ()
  "Create and initialize the FP buffer.

Returns the fp-buffer initialized."
  (with-current-buffer (get-buffer-create fp-buffer-name)
    (let ((inhibit-read-only t))
      (delete-region (point-min) (point-max))
      
      ;; (fp-buffer-mode)

      (if fp-buffer-in-buffer
	      (switch-to-buffer-other-window (current-buffer))
	    (switch-to-buffer-other-frame (current-buffer)))
      
      (fp-results-mode)

      (when fp-buffer-last-command
	    (insert fp-buffer-last-command))
    
      (current-buffer))))

(defun fp-buffer-show ()
  "Show the FP buffer."
  (interactive)
  ;; TODO: Is it really good to see the buffer in other window...
  ;; It is maybe good to have more logic...
  (let ((buffy (get-buffer fp-buffer-name)))
    (if buffy
        (switch-to-buffer-other-window buffy)
      (message "FP: No buffer results to show."))))

(defun fp-buffer-insert-element (elt)
  "Insert a formated string into the buffer.
ELT can be:
- `fp-dectuple'
- `fp-filetuple'
- `fp-project'
- `fp-pack'
The fp-buffer-format-X function specify the string to be inserted for
type X."
  (insert (cond ((fp-dectuple-p elt)
                 (fp-buffer-format-dec elt))
                ((fp-filetuple-p elt)
                 (fp-buffer-format-file elt))
                ((fp-project-p elt)
                 (fp-buffer-format-project elt))
                ((fp-pack-p elt)
                 (fp-buffer-format-pack elt))
                (t ""))))

(defun fp-buffer-insert-all (list &optional clear)
  "Insert a list of elements.
LIST is a list of elements that can be anything supported by
`fp-buffer-insert-element'."
  (let ((inhibit-read-only t))
    (with-current-buffer (fp-buffer-create)
      
      (when clear
        (erase-buffer))
      
      (save-excursion
        (dolist (elt list)
	  (fp-buffer-insert-element elt))))))

(defun fp-buffer-format-dec (dec)
  "Insert a dectuple structure in the buffer.
DEC is a `fp-dectuple' instance."
    (format "🖎 %s ⚙ (%s)\n🗨 %s\n📂 %s : 📌 %d\n"
	    (fp-dectuple-name dec)
	    (fp-dectuple-args dec)
	    (replace-regexp-in-string "\\\\n" "\n🗨 " (fp-dectuple-desc dec))
	    (fp-dectuple-file dec)
	    (fp-dectuple-line dec)))

(defun fp-buffer-format-file (file)
  "Insert a filetuple structure in the buffer.
FILE is a `fp-filetuple' structure."
  (format "📄 %s\n"
          (fp-filetuple-filepath file)))

(defun fp-buffer-format-project (project)
  "Insert the given fp-project structure in the buffer.
PROJECT is an `fp-project' structure."
  (format "📁 %s (%s)\n"
          (fp-project-name project)
          (fp-project-language project)))

(defun fp-buffer-format-pack (pack)
  "Insert the given fp-pack structure in the buffer.
PACK is an `fp-pack' structure."
  (format "📦 %s (%s)\n"
          (fp-pack-name pack)
          (substring (fp-pack-filepath pack) -25)))

(defun fp-buffer-next-dec ()
  "Go to the next declaration."
  (interactive)
  (goto-char (point-at-eol))
  (search-forward "🖎" nil t))

(defun fp-buffer-prev-dec ()
  "Go to the previous declaration."
  (interactive)
  (goto-char (point-at-bol))
  (search-backward "🖎" nil t))

(defun fp-buffer-goto-dec ()
  "Search for the file and line of the declaration at point.
In the current point declaration, search for the file path and line number
and open it using `find-file' at the same line number."
  (interactive)
  (goto-char (point-at-bol))
  (if (search-forward-regexp "📂 \\(.*\\) : 📌 \\([[:digit:]]+\\)" nil t)
      (let ((path (match-string-no-properties 1))
	    (line (string-to-number (match-string-no-properties 2))))
	(other-frame 1)
	(find-file path)
	(goto-line line))
    (message "No path-line founded.")))

(defun fp-buffer-update ()
  "Update the buffer contents."
  (interactive)
  (if fp-buffer-last-command
      (with-current-buffer (fp-buffer-create)
	(goto-char (point-min))
	(eval-region (point-min) (point-at-eol))
	(message "fp-buffer updated")) ;; progn
    (message "No last command setted in `fp-buffer-last-command'.")))

;; __________________________________________________

(defun fp-buffer-set-hooks ()
  "Set needed hooks for update the buffer acordingly."
   (unless (member fp-buffer-update find-file-hook)
     (add-hook 'find-file-hook fp-buffer-update))
   (unless (member fp-buffer-scan-and-update after-save-hook)
     (add-hook 'after-save-hook fp-buffer-scan-and-update)))

(provide 'fp-buffer)
;;; fp-buffer.el ends here
