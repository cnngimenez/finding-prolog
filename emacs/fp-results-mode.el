;;; fp-results-mode.el --- FP Results major mode  -*- lexical-binding: t; -*-

;; Copyright 2017 Gimenez, Christian
;;
;; Author: Giménez, Christian
;; Version:  0.1
;; Keywords: tools, convenience
;; URL: https://gitlab.com/cnngimenez/finding-prolog
;; Package-Requires: ((emacs "27.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:
;;
;; Define a major mode with faces and key bindings to show results.
;;

;;; Code:

(defface fp-results-decname-face
  '((t
     :underline t
     :inherit font-lock-warning-face))
  "Face for description of dectuples.")

(defface fp-results-desc-face
  '((t
     :height 0.75
     :background "gray20"
     :inherit font-lock-doc-face))
  "Face for description of dectuples.")

(defface fp-results-params-face
  '((t
     :height 0.75
     :inherit font-lock-doc-face))
  "Face for description of dectuples.")

(defface fp-results-path-face
  '((t
     :height 0.75
     :inherit font-lock-comment-face))
  "Face for description of dectuples.")

(defface fp-results-linenum-face
  '((t
     :height 0.75
     :foreground "green"
     :inherit font-lock-doc-face))
  "Face for description of dectuples.")

(defface fp-results-icon-face
  '((t
     :inherit font-lock-warning-face))
  "Face for description of dectuples.")

(defface fp-results-pack-face
  '((t
     :height 1.25
     :weight bold))
  "Face for pack names.")

(defvar fp-results-font-lock-keywords
  '(("^\\(🖎\\) \\(.*\\) \\(⚙\\) \\(.*\\)"
     (1 'fp-results-icon-face t)
     (2 'fp-results-decname-face t)
     (3 'fp-results-icon-face t)
     (4 'fp-results-params-face t))
    ("^\\(🗨\\) \\(.*\\)$"
     (1 'fp-results-icon-face t)
     (2 'fp-results-desc-face t))
    ("\\(📂\\) \\(.*\\) : \\(📌\\) \\(.*\\)"
     (1 'fp-results-icon-face t)
     (2 'fp-results-path-face t)
     (3 'fp-results-icon-face t)
     (4 'fp-results-linenum-face t))
    ("\\(📦\\) \\(.*\\) (\\(.*\\))"
     (1 'fp-results-icon-face t)
     (2 'fp-results-pack-face t)
     (3 'fp-results-path-face t)))
  "Font-lock keywords for `fp-results-mode'.")

(defvar fp-results-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "g" 'fp-buffer-update)
    (define-key map "n" 'fp-buffer-next-dec)
    (define-key map "p" 'fp-buffer-prev-dec)
    (define-key map "\r" 'fp-buffer-goto-dec)
    ;; help
    (define-key map "h" 'describe-mode)
    (define-key map "?" 'describe-mode)
    map)
  "Keymap for `fp-results-mode'.")

(define-derived-mode fp-results-mode nil "FP Results"
  "Major mode for the FP results buffer."
  (setq-local font-lock-defaults '(fp-results-font-lock-keywords nil t))
  (setq-local buffer-read-only t))

(provide 'fp-results-mode)
;;; fp-results-mode.el ends here
