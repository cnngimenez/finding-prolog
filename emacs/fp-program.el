;;; fp-program.el --- Call the fp.pl script  -*- lexical-binding: t; -*-

;; Copyright 2023 Christian Gimenez
;;
;; Author: Christian Gimenez
;; Maintainer: Christian Gimenez
;; Version: 0.1.0
;; Keywords: tool, convenience
;; URL: https://gitlab.com/cnngimenez/finding-prolog
;; Package-Requires: ((emacs "27.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Run the fp.pl script and parse its results.

;;; Code:

(require 'cl-lib)

(defgroup fp-program nil
  "Finding Prolog Program."
  :tag "Finding Prolog Program"
  :version "Emacs 24.5.1"
  :group 'fp-main)

(defcustom fp-program-path "~/finding-prolog/fp.pl"
  "The path of the fp.pl script."
  :tag "Path of the fp.pl script"
  :group 'fp-program
  :type 'file)

(cl-defstruct fp-dbconfig lang projname)

(defvar fp-program-current-dbconf nil
  "The current `fp-dbconfig' instance to use.")

(defvar fp-program-current-callback nil
  "The callback function to use when the fp.pl process ends.

It receives the process output string to parse.")

(defvar fp-program-process nil
  "The current process.")

(defconst fp-program-buffer " *fp-program-process*"
  "The buffer name where the output of fp.pl is stored.")

(defun fp-program-process-filter (process string)
  "Filter function to process the output of the fp.pl program.

PROCESS is the fp.pl process.
STRING is the output string.

Call the `fp-program-current-callback' with the process output, only
if the process has finished."
  (with-current-buffer (process-buffer process)
    (goto-char (point-max))
    (insert string)
  
    (when (equal (process-status process) 'exit)
      (funcall fp-program-current-callback
               (buffer-substring-no-properties (point-min) (point-max))))))

(defun fp-program-process-sentinel (process event)
  "Sentinel for the fp.pl process.

PROCESS the fp.pl running process.
EVENT is the string with the event to act.

This sentines is used by `fp-program.start'."
  (when (string= event "finished\n")
    (funcall fp-program-current-callback
             (with-current-buffer (process-buffer process)
               (buffer-substring-no-properties (point-min) (point-max))))))

(defun fp-program-dbconf-to-parameters (dbconf)
  "Convert DBCONF to a list of fp.pl parameters."
  (list "-l" (format "%s" (fp-dbconfig-lang dbconf))
        "-p" (format "%s" (fp-dbconfig-projname dbconf))))

(defun fp-program--parameters (parameters &optional dbconf)
  "Generate the parameter list.
PARAMETERS must be a list of user parameters.
DBCONF is an `fp-dbconfig' structure.  If not provided, use
`fp-program-current-dbconf'."
  (append parameters
          (if dbconf
              (fp-program-dbconf-to-parameters dbconf)
            (fp-program-dbconf-to-parameters fp-program-current-dbconf))))

(defun fp-program-start (callback &optional parameters dbconf)
  "Start the fp.pl program asnynchronously.

Start the fp.pl script with the given PARAMETERS list and the DBCONF
provided.  Call CALLBACK with the output string as soon it ends.

If DBCONF is not provided, use `fp-program-current-dbconf'.

PARAMETERS must be a list of strings.  If not provided, nil is used.

The `fp-program-process' variable is updated with the current process.
The `fp-program-current-callback' is used to store the CALLBACK
function.  This means that only one asynchronous process should be
executed at a time.

The constant `fp-program-buffer' is used to set the process buffer
with its output.

Return the process created."
  (setq fp-program-current-callback callback)
  
  (when (and fp-program-process
             (equal (process-status fp-program-process) 'run))
    (message "Warning: fp-program-process is still running.  Another process is going to be started."))

  (with-current-buffer (get-buffer-create fp-program-buffer)
    (erase-buffer))
  
  (setq fp-program-process
        (apply #'start-process "fp-prolog" fp-program-buffer
               fp-program-path
               (fp-program--parameters parameters dbconf)))
  
  ;; (set-process-filter fp-program-process #'fp-program-process-filter)
  (set-process-sentinel fp-program-process #'fp-program-process-sentinel)
  
  fp-program-process)

;; --------------------------------------------------
;; Functions to call fp.pl easier.
;; --------------------------------------------------

(defun fp-program-list-projects (callback &optional dbconf)
  "Return the project list from fp.pl.

Call CALLBACK with the output string when the program ends.

DBCONF is an `fp-dbconfig' structure.  If not provided, use
`fp-program-current-dbconf'.  It is supposed to not to work
for listing projects."
  (fp-program-start callback '("-L") dbconf))

(defun fp-program-list-packages (callback &optional dbconf)
  "Return the package list from fp.pl.

Call CALLBACK with the output string when the program ends.

DBCONF is an `fp-dbconfig' structure.  If not provided, use
`fp-program-current-dbconf'."
  (fp-program-start callback '("-q" "listpacks") dbconf))

(defun fp-program-scan (file-path &optional callback dbconf)
  "Scan the given FILE-PATH asynchronously.

Call CALLBACK when the program ends.

DBCONF is a `fp-dbconfig' structure.  If not provided, use
`fp-program-current-dbconf'."
  (let ((fnc-callback (if callback
                          callback
                        (lambda () (message "FP: Scan finished.")))))
    (fp-program-start fnc-callback
                      (list "-s" file-path)
                      dbconf)))

(defun fp-program-scan-recursive (path wildcards &optional callback dbconf)
  "Scan the given PATH recursively.

PATH is the directory path to scan recursivel.
WILDCARDS is a string with the file wildcards that has to match (ex.:
\"*.pl\" for prolog files).

Call CALLBACK when the program ends if it is present.
DBCONF is a `fp-dbconfig' structure.  If not provided, use
`fp-program-current-dbconf'."
  (let ((fnc-callback (if callback
                          callback
                        (lambda () (message "FP: Scan finished.")))))
    (fp-program-start fnc-callback
                      (list "-s" "-r" path wildcards)
                      dbconf)))

(defun fp-program-query-files (callback &optional dbconf)
  "Query for all the files in the project database.

Call CALLBACK with the output string when the program ends.

DBCONF is a `fp-dbconfig' structure.  If not provided, use
`fp-program-current-dbconf'."
  (fp-program-start callback
                    (list "-q" "files")
                    dbconf))

(defun fp-program-query-pack (package-name callback &optional dbconf)
  "Query for all the files in the project database.

PACKAGE-NAME is the package or module name to query.
Call CALLBACK with the output string when the program ends.

DBCONF is a `fp-dbconfig' structure.  If not provided, use
`fp-program-current-dbconf'."
(fp-program-start callback
                  (list "-q" "pack" package-name)
                  dbconf))

(defun fp-program-query-start (starting-string callback &optional dbconf)
  "Query for all the files in the project database.

STARTING-STRING is the prefix string to search.
Call CALLBACK with the output string when the program ends.

DBCONF is a `fp-dbconfig' structure.  If not provided, use
`fp-program-current-dbconf'."
(fp-program-start callback
                  (list "-q" "start" starting-string)
                  dbconf))

(defun fp-program-query-substring (substring callback &optional dbconf)
  "Query for all the files in the project database.

SUBSTRING is the substring to search.
Call CALLBACK with the output string when the program ends.

DBCONF is a `fp-dbconfig' structure.  If not provided, use
`fp-program-current-dbconf'."
(fp-program-start callback
                  (list "-q" "substring" substring)
                  dbconf))

(provide 'fp-program)
;;; fp-program.el ends here
