;;; fp-mode.el --- 

;; Copyright 2016 Giménez, Christian
;;
;; Author: Giménez, Christian
;; Version: $Id: fp-mode.el,v 0.0 2016/08/20 20:46:46 christian Exp $
;; Keywords: 
;; X-URL: not distributed yet

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require 'fp-mode)

;;; Code:

(provide 'fp-mode)
(eval-when-compile
  (require 'cl))

(require 'fp-process)
(require 'fp-project)
					; __________________________________________________
					; font-lock

(defface fp-dec-face
  '((t :inherit font-lock-doc-face))
  "Face for dec keywords.")

(defvar fp-font-lock-keywords
  '(
    ("^ERROR:.*$" . font-lock-warning-face)
    ("^\\?-.*$" . font-lock-comment-face)
    ("^true.*$" . font-lock-comment-face)
    ("^\\(dec\\)(\\([^,]+\\),"
     (1 'fp-dec-face)
     (2 'font-lock-keyword-face))
    )
  "Font-lock keywords for `fa-mode'"
  )

					; __________________________________________________
					; Keymaps
(defvar fp-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "n" 'fp-next-dec)
    (define-key map "p" 'fp-prev-dec)
    (define-key map "\r" 'fp-goto)
    map)
  "Keymap for `fp-mode'.")

					; __________________________________________________
					; Global keymaps

(global-set-key "\C-xp" (make-sparse-keymap)) ;; define prefix
(global-set-key "\C-xpp" 'fp-query-search-dec-at-point)
(global-set-key "\C-xp\C-s" 'fp-query-starting-with)
(global-set-key "\C-xpq" 'fp-return-buffer)
(global-set-key "\C-xps" 'fp-project-scan)
(global-set-key "\C-xpo" 'fp-show-output)

					; __________________________________________________
					; Main Function

(define-derived-mode fp-mode nil "Finding-Prolog"
  "Major mode for Finding Prolog."
  (setq-local buffer-read-only t)
  (setq-local font-lock-defaults '(fp-font-lock-keywords nil t))
  (setq-local buffer-read-only t)
  )


;;; fp-mode.el ends here
