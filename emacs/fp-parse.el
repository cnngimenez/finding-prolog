;;; fp-parse.el --- Parser functions for fp.pl output  -*- lexical-binding: t; -*-

;; Copyright 2023 Christian Gimenez
;;
;; Author: Christian Gimenez
;; Maintainer: Christian Gimenez
;; Version: 0.1.0
;; Keywords: tools, convenience
;; URL: https://gitlab.com/cnngimenez/finding-prolog
;; Package-Requires: ((emacs "27.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Functions to parse STRINGS into fp-dectuple, or fp-filetuple.

;;; Code:

(require 'cl-lib)

(cl-defstruct fp-dectuple package name args desc line file)

(cl-defstruct fp-filetuple filepath date)

(cl-defstruct fp-project language name)

(cl-defstruct fp-pack name filepath)

(defconst fp-parse-file-regexp "file('\\([^']*\\)',\\([[:digit:]\\.]*\\))"
  "Regxep to parse file tuples.
The first subexpression must match the file path.
The second subexpression must match the date in numbers.")

(defconst fp-parse-dec-regexp "dec('\\([^']*\\)','\\([^']*\\)','\\([^']*\\)','\\([^']*\\)',\\([[:digit:]]*\\),'\\([^']*\\)')"
  "Regexp to parse dec tuples.")

(defconst fp-parse-project-regexp
  "database(\\([_[:alnum:]]+\\),\\([_[:alnum:]]+\\))"
  "Regexp to parse database/2 tuples (project names and languages).")

(defconst fp-parse-pack-regexp
  "pack(\\(.*\\),'?\\(.*\\)'?)"
  "Regexp to parse pack/2 tuples (package names and languages).")

(defun fp-parse-pack (str-line)
  "Parse pack tuples in an output line.
STR-LINE is a string with only one line data."
  (when (string-match fp-parse-pack-regexp str-line)
    (make-fp-pack :name (match-string-no-properties 1 str-line)
                  :filepath (match-string-no-properties 2 str-line))))

(defun fp-parse-all-packs (str)
  "Parse pack tuples from a complete output.
STR must be the complete output from fp.pl."
  (delq nil (mapcar #'fp-parse-pack (split-string str "\n" t "[[:space:]]*"))))

(defun fp-parse-file (str-line)
  "Parse file tuples in an output line.
STR-LINE is a string with only one line data."
  (when (string-match fp-parse-file-regexp str-line)
    (make-fp-filetuple :filepath (match-string-no-properties 1 str-line)
                       :date (match-string-no-properties 2 str-line))))

(defun fp-parse-all-files (str)
  "Parse file tuples from a complete output.
STR must be the complete output from fp.pl."
  (delq nil (mapcar #'fp-parse-file (split-string str "\n" t "[[:space:]]*"))))

(defun fp-parse-dec (str-line)
  "Parse a dec tuple from an output line.
STR-LINE is a string with only one line data."
  (when (string-match fp-parse-dec-regexp str-line)
    (make-fp-dectuple :package (match-string-no-properties 1 str-line)
                      :name (match-string-no-properties 2 str-line)
                      :args (match-string-no-properties 3 str-line)
                      :desc (match-string-no-properties 4 str-line)
                      :line (string-to-number (match-string-no-properties 5 str-line))
                      :file (match-string-no-properties 6 str-line))))

(defun fp-parse-all-decs (str)
  "Parse dec tuples from a complete output.
STR must be the output from fp.pl."
  (delq nil (mapcar #'fp-parse-dec (split-string str "\n" t "[[:space:]]*"))))


(defun fp-parse-project (str-line)
  "Parse a database/2 tuple from an output line.
STR-LINE is the string with only one line of data."
  (when (string-match fp-parse-project-regexp str-line)
    (make-fp-project :language (match-string-no-properties 1 str-line)
                     :name (match-string-no-properties 2 str-line))))

(defun fp-parse-all-projects (str)
  "Parse database/2 tuples from a complete output.
STR must be the output from fp.pl."
  (delq nil (mapcar #'fp-parse-project (split-string str "\n" t "[[:space:]]*"))))

(provide 'fp-parse)
;;; fp-parse.el ends here
