;;; company-fp.el --- Company support for Finding Prolog  -*- lexical-binding: t; -*-

;; Copyright 2018 Gimenez, Christian
;;
;; Author: Gimenez, Christian
;; Version: 0.1
;; Keywords: tools, convenience
;; URL: https://gitlab.com/cnngimenez/finding-prolog
;; Package-Requires: ((emacs "27.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:
;;
;; Provide functions and a backend for Company.  This is useful to
;; allow autocompletions!

;;; Code:

(require 'cl-lib)
(require 'company)
(require 'fp-program)
(require 'fp-parse)

(defvar company-fp-completions nil
  "Completions list obtained from the FP subprocess.")

(defvar company-fp-prev-buffer nil
  "The previous buffer used by company-fp.")

(defun company-fp (command &optional arg &rest ignored)
  "FP Company backend.
The COMMAND argument is as symbol: interactive, prefix or candidates.
See `company-backends' for more information.
ARG is the parameter provided by the backend, usually, the prefix to
be completed.
IGNORED parameters are not used.

No completions will be suggested if there is no current project (or database)
configuration set.  Use `fp-set-current-project' from fp.el to set the current project
and database."
  (interactive (list 'interactive))

  (cl-case command
    (interactive (company-begin-backend 'company-fp))
    (prefix (and fp-program-current-dbconf
                 (company-grab-symbol)))
    (candidates
     (progn
       (company-fp-get-completions arg)
       (company-fp--get-completions-names arg company-fp-completions)))
    (meta (company-fp-get-meta arg company-fp-completions))
    (doc-buffer (company-fp--get-buffer arg company-fp-completions))))

(defun company-fp--get-buffer (name lst-decs)
  "Search for NAME in given decs and return a buffer with its description.
LST-DECS is a list of fp-decs.

Return the name of the help buffer."
  (with-current-buffer (get-buffer-create "*fp-company-help*")
    (erase-buffer)
    (let ((selected-decs (cl-remove-if-not (lambda (dec)
                                             (string-equal name (fp-dectuple-name dec)))
                                           lst-decs)))
      (insert (mapconcat (lambda (dec)
                           "Generate a string with DEC details."
                           (format "(📦 %s): ⚙ %s\n    %s\n%s\n📍%s:%s"
                                   (fp-dectuple-package dec)
                                   (propertize (fp-dectuple-name dec)
                                               'face 'font-lock-function-name-face)
                                   (fp-dectuple-args dec)
                                   (fp-dectuple-desc dec)

                                   (fp-dectuple-file dec)
                                   (fp-dectuple-line dec)))
                         selected-decs
                         "\n-----\n")))
    "*fp-company-help*"))

(defun company-fp--get-completions-names (prefix decs)
  "Return the completions names that matches the given prefix.
DECS is a list of fp-decs.
PREFIX is a string that the name must match."
  (delete-dups
   (cl-remove-if-not
    (lambda (c)
      "Return t if C has the prefix ARG."
      (string-prefix-p prefix c))
    (cl-mapcar (lambda (dec)
	         "Obtain the function name"
	         (fp-dectuple-name dec))
	       decs))))

(defun company-fp-get-completions (prefix)
  "Get the completions from the subprocess."
  (interactive)
  (setq company-fp-prev-buffer (current-buffer))
  (fp-program-query-start prefix
                          #'company-fp-populate-completions))
  
(defun company-fp-populate-completions (str-response)
  "Populate the `company-fp-completions' with the subprocess results.
Ensure to set `company-fp-prev-buffer' for activating the company
backend.
STR-RESPONSE is the subprocess result."
  (setq company-fp-last-query (list (current-word) str-response))
  (setq company-fp-completions (fp-parse-all-decs str-response))
  company-fp-completions)

(defun company-fp-get-meta (name lst-decs)
  (mapconcat (lambda (dec)
               (if (string-empty-p (fp-dectuple-desc dec))
                   "No description"
                 (fp-dectuple-desc dec)))
             (cl-remove-if-not (lambda (dec)
                                 (string-equal name (fp-dectuple-name dec)))
                               lst-decs)
             "\n"))

(add-to-list 'company-backends #'company-fp)

(defun company-fp-do-completions ()
  "Do search and show the completions."
  (interactive)
  (company-fp-get-completions)
  (call-interactively 'company-fp))

(provide 'company-fp)
;;; company-fp.el ends here
