;;; fp.el --- Finding Prolog  -*- lexical-binding: t; -*-

;; Copyright 2016 Gimenez, Christian
;;
;; Author: Gimenez, Christian
;; Version: 0.1
;; Keywords: tools, convenience
;; URL: https://gitlab.com/cnngimenez/finding-prolog
;; Package-Requires: ((emacs "27.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:
;; Finding Prolog (FP). Index and find subprograms declarations.

;;; Code:

(defgroup fp nil
  "Finding Prolog."
  :tag "Finding Prolog"
  :version "Emacs 24.5.1"
  :group 'editing)

(require 'cl-lib)

(require 'fp-program)
(require 'fp-parse)
(require 'fp-buffer)

(defcustom company-fp-major-modes-supported
  '((alight-mode . ada)
    (emacs-lisp-mode . elisp)
    (lisp-interaction-mode . elisp)
    (haskell-mode . haskell)
    (tex-mode . latex)
    (latex-mode . latex)
    (php-mode . php)
    (web-mode . php)
    (prolog-mode . prologlang)
    (python-mode . python)
    (ttl-mode . turtle)
    (coffee-mode . coffee))
  "An alist of modes and their language names.
An alist associating the major mode name and the language name used in the FP project.
This will be used to determine the supported language automatically.")

(defvar fp-return-buffer nil
  "This is the buffer to return when the user calls
`fp-return-buffer'.")

;; --------------------------------------------------
;; Show results
;; --------------------------------------------------

(defun fp-show-dec-results (str-response)
  "Show declaration results.
Parse STR-RESPONSE string, which is supposed to be the fp.pl output of
several dec tuples.  The results show it on the fp-buffer."
  (setq fp-last-respones str-response)
  (fp-buffer-insert-all (fp-parse-all-decs str-response) t)
  (fp-buffer-show))

(defun fp-show-file-results (str-response)
  "Retrieve all the file tuple answers from STR-RESPONSE and show it."
  (setq fp-last-response str-response)
  (fp-buffer-insert-all (fp-parse-all-files str-response) t)  
  (fp-buffer-show))

(defun fp--show-projects (str-response)
  "Parse and show the projects name and languages.
STR-RESPONSE is the output string from fp.pl."
  (fp-buffer-insert-all (fp-parse-all-projects str-response) t)
  (fp-buffer-show))

(defun fp--show-packages (str-response)
  "parse and show the package data.
STR-RESPONSE is the output string from fp.pl."
  (fp-buffer-insert-all (fp-parse-all-packs str-response) t)
  (fp-buffer-show))

;; --------------------------------------------------
;; Scan interactive functions
;; --------------------------------------------------

(defun fp-scan-this-file ()
  "Scan only the current opened file."
  (interactive)
  (fp-scan-file (buffer-file-name (current-buffer))))

(defun fp-scan-file (file)
  "Scan only one FILE and store its declarations into the database."
  (interactive "fFile?")
  (fp-program-scan file))

(defun fp-scan-recursive (path wildcards)
  "Scan all files under the given PATH for declarations and store them in the database.

PATH is the initial path, it will search recursively in all its subdirectories.
WILDCARDS the pattern the file must match. It is not a Regexp pattern, is the common 'ls' pattern.

Example:

    M-x fp-scan-recursive \"~/example\" \"*.el\"

Will scan al elisp files inside the ~/example directory and its subdirectories recursively."
  (interactive "DPath?\nMWildcards?")
  (fp-program-scan-recursive path wildcards))

;; --------------------------------------------------
;; Query interactive functions
;; --------------------------------------------------

(defun fp-list-projects ()
  "Show a list of projects and their languages in a new buffer."
  (interactive)
  (fp-program-list-projects #'fp--show-projects))

(defun fp-list-packages ()
  "Show a list of packages and their files in a new buffer."
  (interactive)
  (fp-program-list-packages #'fp--show-packages))

(defun fp-query-starting-with (str)
  "Search for declarations that starts with STR string.

Return a list of string with the results. "
  (interactive "MStarting with?")
  (setq fp-return-buffer (current-buffer))
  (fp-program-query-start str #'fp-show-dec-results))

(defun fp-query-has-substring (str)
  "Search for declarations that has the STR string as substring.

Return a list of string with the results simmilar to `fp-query-starting-with'."
  (interactive "MSubstring?")
  (setq fp-return-buffer (current-buffer))
  (fp-program-query-substring str #'fp-show-dec-results))

(defun fp-query-search-dec-at-point ()
  "Search declaration starting with the text of the last word at the current point.

Using the last word typed, or the first word before the point, search using `fp-query-starting-with' for declarations starting with that word."

  (interactive)
  (fp-query-starting-with (current-word)))

;; TODO: not implemented!
;; (defun fp-query-file-decs ()
;;   "Ask the user for a file and show all its declarations."
;;   (interactive)
;;   (message "Not implemented yet!"))

(defun fp-query-file-decs-in-buffer ()
  "Same as `fp-query-file-decs' but don't use Helm, instead use the fp-buffer
for more details and permanent view."
  (interactive)
  (fp-process-query-file-decs #'fp-show-dec-results))

;; TODO: not implemented!
;; (defun fp-query-this-file-decs ()
;;   "Show all file's declarations. Use this file."
;;   (interactive)
;;   (fp-query-file-decs-int (buffer-file-name)))

(defun fp-query-list-files ()
  "Show all files in the current database"
  (interactive)
  (fp-program-query-files #'fp-show-file-results))

;; --------------------------------------------------
;; Convenience interactive functions
;; --------------------------------------------------

(defun fp-set-current-project (lang project)
  "Set the current project to use.

Set `fp-current-dbconf' variable manually."
  (interactive "MLanguage?\nMProject name?")
  (setq fp-program-current-dbconf (make-fp-dbconfig :lang lang
                                                    :projname project)))

(defun fp-current-project ()
  "Show the current project.

Show `fp-current-dbconf' to the user."
  (interactive)
  (if fp-program-current-dbconf
      (message "Current project is: \"%s\" (programming language: \"%s\")."
               (fp-dbconfig-projname fp-current-dbconf)
               (fp-dbconfig-lang fp-current-dbconf))
    (message "No current project set.")))

(defun fp-return-buffer ()
  "Return to the buffer where an fp-* command was called."
  (interactive)
  (if fp-return-buffer
      (switch-to-buffer fp-return-buffer nil t)
    (message "No return buffer stored.")))

(provide 'fp)
;;; fp.el ends here
